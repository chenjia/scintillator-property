function output= gauss (param,x)
m = param(1);
s = param(2);
N = param(3);
t_r=param(4);
t_d=param(5);
tsk=param(6);
R=param(7);
c=param(8);
% c=0;

% hilf=x-m;
%output = m*hilf.^3 + s*x.^2 + N*x.^1;
% this is the 3rd order polynomial equation here
% output = 1/(N*sqrt(2*pi*s^2))*exp(-(x-m)^2/(2*s^2))+c;


% output = N*exp(-(x - m).^2/(2*s^2))+R*(exp(-(x-(tsk))/t_d)-exp(-(x-(tsk))/t_r)).*heaviside(x-(tsk))+c;
% output = N*exp(-(x - m).^2/(2*s^2))+R*(exp(-(x-(m))/t_d)-exp(-(x-(m))/t_r)).*heaviside(x-(m))+c;
output = N*exp(-(x - m).^2/(2*s^2))+c;
%output=normpdf(x,m,s);
end