import numpy as np

def gauss(param, t):
    # common part
    tsk = param[0]
    R = param[1]
    R2 = param[2]
    R3 = param[3]
    t_d = param[4]
    t_d2 = param[5]
    t_d3 = param[6]
    t_r = param[7]
    t_r2 = param[8]
    t_r3 = param[9]
    CAmp = param[10]

    # Prompt photons part
    tskpr = 0
    sigma_min = 0.01  # A sigma as small as possible for the prompt photons. Smaller than a bin

    # common part
    gpd = np.where(t > tsk)[0]

    # --- 1 tau_rise, 3 tau_decay
    zw = np.zeros(len(t))
    zw[gpd] = (R / (t_d - t_r) * (np.exp(-(t[gpd] - tsk) / t_d) - np.exp(-(t[gpd] - tsk) / t_r)) +
               R2 / (t_d2 - t_r) * (np.exp(-(t[gpd] - tsk) / t_d2) - np.exp(-(t[gpd] - tsk) / t_r)) +
               R3 / (t_d3 - t_r) * (np.exp(-(t[gpd] - tsk) / t_d3) - np.exp(-(t[gpd] - tsk) / t_r))) / (R + R2 + R3)

    # --- 2 tau_rise, 2 tau_decay
    # zw = np.zeros(len(t))
    # zw[gpd] = (R / (t_d - t_r) * (np.exp(-(t[gpd] - tsk) / t_d) - np.exp(-(t[gpd] - tsk) / t_r)) +
    #            R2 / (t_d2 - t_r2) * (np.exp(-(t[gpd] - tsk) / t_d2) - np.exp(-(t[gpd] - tsk) / t_r2))) / (R + R2)

    # --- 2 tau_rise, 3 tau_decay
    # zw = np.zeros(len(t))
    # zw[gpd] = (R / (t_d - t_r) * (np.exp(-(t[gpd] - tsk) / t_d) - np.exp(-(t[gpd] - tsk) / t_r)) +
    #            R2 / (t_d2 - t_r2) * (np.exp(-(t[gpd] - tsk) / t_d2) - np.exp(-(t[gpd] - tsk) / t_r2)) +
    #            R3 / (t_d3 - t_r) * (np.exp(-(t[gpd] - tsk) / t_d3) - np.exp(-(t[gpd] - tsk) / t_r))) / (R + R2 + R3)

    # --- 3 tau_rise, 3 tau_decay
    # zw = np.zeros(len(t))
    # zw[gpd] = (R / (t_d - t_r) * (np.exp(-(t[gpd] - tsk) / t_d) - np.exp(-(t[gpd] - tsk) / t_r)) +
    #            R2 / (t_d2 - t_r2) * (np.exp(-(t[gpd] - tsk) / t_d2) - np.exp(-(t[gpd] - tsk) / t_r2)) +
    #            R3 / (t_d3 - t_r3) * (np.exp(-(t[gpd] - tsk) / t_d3) - np.exp(-(t[gpd] - tsk) / t_r3))) / (R + R2 + R3)

    # --- 1 tau_rise, 2 tau_decay + C_amp
    # zw = np.zeros(len(t))
    # zw[gpd] = (CAmp / (sigma_min * np.sqrt(2 * 3.1415)) * np.exp(-(((t[gpd] - tsk) ** 2) / (2 * sigma_min ** 2))) +
    #            (R / (t_d - t_r) * (np.exp(-(t[gpd] - tsk) / t_d) - np.exp(-(t[gpd] - tsk) / t_r)) +
    #             R2 / (t_d2 - t_r) * (np.exp(-(t[gpd] - tsk) / t_d2) - np.exp(-(t[gpd] - tsk) / t_r)))) / (R + R2 + CAmp)

    # --- 2 tau_rise, 2 tau_decay + C_amp
    # zw = np.zeros(len(t))
    # zw[gpd] = (CAmp / (sigma_min * np.sqrt(2 * 3.1415)) * np.exp(-(((t[gpd] - tsk) ** 2) / (2 * sigma_min ** 2))) +
    #            (R / (t_d - t_r) * (np.exp(-(t[gpd] - tsk) / t_d) - np.exp(-(t[gpd] - tsk) / t_r)) +
    #             R2 / (t_d2 - t_r2) * (np.exp(-(t[gpd] - tsk) / t_d2) - np.exp(-(t[gpd] - tsk) / t_r2)))) / (R + R2 + CAmp)

    # --- 1 tau_rise, 3 tau_decay + C_amp
    # zw = np.zeros(len(t))
    # zw[gpd] = (CAmp / (sigma_min * np.sqrt(2 * 3.1415)) * np.exp(-(((t[gpd] - tsk) ** 2) / (2 * sigma_min ** 2))) +
    #            (R / (t_d - t_r) * (np.exp(-(t[gpd] - tsk) / t_d) - np.exp(-(t[gpd] - tsk) / t_r)) +
    #             R2 / (t_d2 - t_r) * (np.exp(-(t[gpd] - tsk) / t_d2) - np.exp(-(t[gpd] - tsk) / t_r)) +
    #             R3 / (t_d3 - t_r) * (np.exp(-(t[gpd] - tsk) / t_d3) - np.exp(-(t[gpd] - tsk) / t_r)))) / (R + R2 + R3 + CAmp)

    output = zw / np.sum(zw)

    return output
