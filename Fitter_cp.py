import numpy as np
import matplotlib.pyplot as plt
from DCF2 import gauss as DCF2
from UpdateParameters import UpdateParameters
from fit_baseline import smooth
import pandas as pd
from math import log10

######################################################################
######################################################################
######################################################################
######################################################################
# PART1: INITIALIZATION OF PARAMETERS ################################
######################################################################
######################################################################
######################################################################
######################################################################


# File paths and titles
cry_ID = '2023_Sipad_GAGG/gagg2_t6_4497_420longpass_500khz.txt'
plottitle = 'GAGG2_2x2x3_t6_4497_420longpass_500kHz'
plotPrefix = 'plots/GAGG2_t6_4497_420longpass_500kHz'

# Seed for random number generator
np.random.seed(6)

# Filename of the IRF as txt file. Used only if IRFGoN==2
IRF_filename = "./IRFs/IRF_HPMT_XRays_Conv_2021_12.txt"

# Bench format
benchFormat = 1    # 0 511keV format, with data in [s], '\n' delimiter, and energy file
                   # 1 x-ray format, with data in adc units, space delimiter, and no energy file.

# Conversion factor from [tdc units] to [ns], only used if benchFormat = 1
x_ray_ConvFactor = 0.0130208333

# Possibility to subtract a histogram of another measurement
# (background?) from the data histogram. Use it with caution.
subtractHisto = 0
histogram_to_subtract = "/Users/loris/Documents/CERN_Doctoral/Benches/Data/RisenDecayXRays/BGSO_Early2021/NoSample_220221_HV70_500kHz_Th130_longTest_start_16.49_stop_11.47.txt"
# histogram_to_subtract = ""

# Fit options
onlyVisualization = 0      # stop before the fit loop

# WARNING: Normalisation of the abundancies is done as:
# iChma(n)/(iChma(2)+iChma(3)+iChma(4)+iChma(11)) so fix them at 0 if you
# do NOT need them or the abundancies will be wrong!!!
#              tsk,    R1,     R2,     R3,     tau_d1,     tau_d2,     tau_d3,     tau_r1,     tau_r2,     tau_r3,     C_amp
iFitIt = np.array([1,     1,      1,      1,          1,          1,         1,          1,          0,          0,          0], dtype=bool)
iChma = np.array([40,   0.06,   0.68,   0.26,        10,       40,         200,       0.020,        0,          0,          0])

# Fit range configuration
rangedec = 650
rangerise = [39, 55]

basfac = 1

lowEcut = 30
highEcut = 100

# WARNING: currently with X-Rays data there is a bug using a bin
# width different from the intrinsic TDC binning. Strong aliasing even using multiples of it. Use 0.0130208333.
# Time binning
t_binWidth = 0.0130208333
t_min = 0
t_max = 650
t_min_baseline = 0
t_max_baseline = 30
t_offset = 0

# IRF options
IRFGoN = 2    # 1: IRF Gaussian then, 2: X-Rays conv HPMT_Laser with TGraphs , 3: combined IRF Pbf2 2x2x3, 4: IRF PbF2 2x2x30 transversal  l   5: FrankenFit  6 FrankenFit 2.0 PbF2 blackpainted    7 FrankenFit 3.0 PbF2 naked

# Total number of runs
RUNStot = 50


######################################################################
######################################################################
######################################################################
######################################################################
# PART2 : READ DATA ##################################################
######################################################################
######################################################################
######################################################################
######################################################################

if benchFormat == 0:
    # --- 511 keV bench file format ---
    print('FORMAT USED: 511 keV bench')
    time_data = np.loadtxt(cry_ID, delimiter='\n') - t_offset  # column vector
    # time_data = np.loadtxt(cry_ID, delimiter='\n')*1e-9 - t_offset  # column vector
    print(time_data.shape)

    # --- read energies
    cry_ID2 = cry_ID[:-4]
    energies = np.loadtxt(cry_ID2 + '_EnergyCh1.txt', delimiter='\n')

    plt.figure()
    energies2 = energies[energies != 0]
    h_energy = plt.hist(energies2, bins=np.arange(-2, 100.1, 0.1))
    # b[1]=0;
    # plot(h_energy);
    plt.xlim(0, 60)
    plt.ylim(0, max(h_energy[0]) * 1.1)
    plt.grid(True)
    plt.axvline(lowEcut, color='r', linewidth=3)
    plt.axvline(highEcut, color='r', linewidth=3)
    plt.pause(5)
    plt.savefig(cry_ID + 'AmplitudeStop.jpg')

    selected_energies = np.where((energies >= lowEcut) & (energies <= highEcut))[0]  # Cut on the energy
    data = time_data[selected_energies]  # Good data selected on the energy

elif benchFormat == 1:
    # --- X-Rays bench file format ---
    print('FORMAT USED: X-Rays bench')
    data_temp = np.loadtxt(cry_ID, delimiter=' ') * x_ray_ConvFactor - t_offset  # column vector
    print(data_temp.shape)
    plt.pause(3)
    data = data_temp

else:
    print('FORMAT NOT RECOGNIZED. STOP')
    plt.pause(3)
    exit()  # Stop it. Just to be safe.

# Histogram timestamps.
# h_time = histogram(data,t_min:t_binWidth:t_max);
# prova = conv(h_time.BinEdges, [0.5 0.5], 'valid');
# hist = histogram(data_temp1, 'BinWidth', t_binWidth, 'BinLimits', [t_min t_max], 'BinCounts', data_temp2);
# DecayCounts = data_temp2;
# DecayBins = data_temp1;
DecayCounts, DecayBins = np.histogram(data, bins=np.arange(t_min, t_max + t_binWidth, t_binWidth))

if subtractHisto == 1:
    print("###WARNING: subtracting a histogram from the data!\n")
    plt.pause(3)
    data_temp = np.loadtxt(histogram_to_subtract, delimiter=' ') * x_ray_ConvFactor - t_offset  # column vector
    DecayCounts_sub, DecayBins_sub = np.histogram(data_temp, bins=np.arange(t_min, t_max + t_binWidth, t_binWidth))
    # DecayBins_sub = DecayBins_sub[DecayBins_sub > t_min & DecayBins_sub < t_max]
    # DecayCounts_sub = DecayCounts_sub[DecayBins_sub > t_min & DecayBins_sub < t_max]
    gpd = np.where((DecayBins_sub > t_min_baseline) & (DecayBins_sub < t_max_baseline))[0]  # for baseline
    cbaseline = np.mean(DecayCounts_sub[gpd]) + 0.001  # Average of the bin content% re-normalization for the minimum baseline
    DecayCounts_sub = (DecayCounts_sub - cbaseline)
    plt.figure()
    plt.plot(DecayBins_sub, DecayCounts_sub)
    DecayCounts = DecayCounts - DecayCounts_sub

DecayCounts[0] = 0
DecayCounts[-1] = 0



#######################################################################
#######################################################################
#######################################################################
#######################################################################
#######################################################################
##########P A R T 3####################################################
#######################################################################
#######################################################################

if IRFGoN == 1:
    # Gaussian IRF
    G = lambda x, p: p[0] * np.exp(-(((x - p[1])**2) / (2 * (p[2]**2))))
    
    xIRF = np.arange(-1, 1, 0.001)
    
    sig = 0.07700
    
    yIRF = G(xIRF, [(1 / (sig * np.sqrt(2 * 3.1413))), 0, sig])
elif IRFGoN == 2:
    # IRF: X-Rays convolved with HPMTandLASER as LUT
    IRF_delay = 0.0   # Shift on the x-axis [ns]
    IRF_data = np.loadtxt(IRF_filename)
    yIRF = np.squeeze(IRF_data[:, 1] / np.sum(IRF_data[:, 1]))
    xIRF = np.squeeze(IRF_data[:, 0] + IRF_delay)
    
    # yIRF = np.interp(np.arange(0, 10, 0.001), xIRF[:, 0], yIRF[:, 0])  # Linear interpolation between min(xIRF) and max(IRF) with step binning
    # xIRF = np.arange(0, 10, 0.001)
elif IRFGoN == 3:
    # IRF with exponential tails rising and falling
    t_d = 0.204
    R = 0.3964
    t_r = 0.483
    R3 = 0.0927
    R2 = 0.5109
    tsk = 2
    
    xIRF = np.arange(0, 10, 0.001)
    t = xIRF
    gpd = np.where(t >= tsk)[0]
    gpd2 = np.where(t < tsk)[0]
    
    sig = 0.001
    zw = np.zeros_like(t)
    zw[gpd] = (R / (t_d) * (np.exp(-(t[gpd] - tsk) / t_d)) + R2 / (sig * np.sqrt(2 * 3.1415)) * np.exp(-(((t[gpd] - tsk) ** 2) / (2 * sig ** 2)))) / (R + R2 + R3)
    zw[gpd2] = (R3 / (t_r) * (np.exp((t[gpd2] - tsk) / t_r)) + R2 / (sig * np.sqrt(2 * 3.1415)) * np.exp(-(((t[gpd2] - tsk) ** 2) / (2 * sig ** 2)))) / (R + R2 + R3)
    
    G = lambda x, p: p[0] * np.exp(-(((x - p[1]) ** 2) / (2 * (p[2] ** 2))))
    
    yIRF = np.convolve(G(xIRF, [1, 5, 0.082 / 2.35]), zw, 'same')
elif IRFGoN == 4:
    # IRF with 2x2x30 PbF2 transversal
    t_d = 0.545
    R = 0.291
    t_r = 0.506
    R3 = 0.1035
    R2 = 0.6054
    tsk = 2
    
    xIRF = np.arange(0, 10, 0.001)
    t = xIRF
    gpd = np.where(t >= tsk)[0]
    gpd2 = np.where(t < tsk)[0]
    
    sig = 0.001
    zw = np.zeros_like(t)
    zw[gpd] = (R / (t_d) * (np.exp(-(t[gpd] - tsk) / t_d)) + R2 / (sig * np.sqrt(2 * 3.1415)) * np.exp(-(((t[gpd] - tsk) ** 2) / (2 * sig ** 2)))) / (R + R2 + R3)
    zw[gpd2] = (R3 / (t_r) * (np.exp((t[gpd2] - tsk) / t_r)) + R2 / (sig * np.sqrt(2 * 3.1415)) * np.exp(-(((t[gpd2] - tsk) ** 2) / (2 * sig ** 2)))) / (R + R2 + R3)
    
    G = lambda x, p: p[0] * np.exp(-(((x - p[1]) ** 2) / (2 * (p[2] ** 2))))
    
    yIRF = np.convolve(G(xIRF, [1, 5, 0.094 / 2.35]), zw, 'same')
elif IRFGoN == 5:
    yIRF_raw = np.array([0.0000000, 0.0020876, 0.0419473, 0.0319824, 0.0369649, 0.0568947, 0.0618772, 0.091772, 0.131632, 0.156544, 0.131632, 0.0668597, 0.0618772, 0.0618772, 0.027, 0.017035, 0.0220175, 0.0000000, 0.000])
    xIRF_raw = np.array([0.00, 0.03, 0.04, 0.06, 0.08, 0.10, 0.12, 0.14, 0.16, 0.18, 0.20, 0.22, 0.24, 0.26, 0.28, 0.30, 0.32, 0.34, 10.])
    
    xIRF = np.arange(0, 10.001, 0.001)
    yIRF = np.interp(xIRF_raw, yIRF_raw, kind='linear')(xIRF)
elif IRFGoN == 6:
    t_d = 0.0479
    t_r = 0.3739
    R = 0.601
    R2 = 0.8341
    R3 = 0.284
    tsk = 5.0
    
    xIRF = np.arange(0, 10.001, 0.001)
    t = xIRF
    gpd = np.where(t >= tsk)[0]
    gpd2 = np.where(t < tsk)[0]
    
    sig = 0.001
    zw = np.zeros(len(t))
    zw[gpd] = (R/t_d * np.exp(-(t[gpd]-tsk)/t_d) + R2/(sig*np.sqrt(2*np.pi))*np.exp(-((t[gpd]-tsk)**2/(2*sig**2)))) / (R+R2+R3)
    zw[gpd2] = (R3/t_r * np.exp((t[gpd2]-tsk)/t_r) + R2/(sig*np.sqrt(2*np.pi))*np.exp(-((t[gpd2]-tsk)**2/(2*sig**2)))) / (R+R2+R3)
    
    G = lambda x, p: p[0]*np.exp(-((x-p[1])**2)/(2*(p[2]**2)))
    yIRF = np.convolve(G(xIRF, [0.01, 2, 0.043]), zw, mode='same')
elif IRFGoN == 7:
    t_d = 5.927
    t_r = 0.156
    R = 0.0289
    R2 = 0.9689
    R3 = 0.0022
    tsk = 5.0
    
    xIRF = np.arange(0, 10.001, 0.001)
    t = xIRF
    gpd = np.where(t >= tsk)[0]
    gpd2 = np.where(t < tsk)[0]
    
    sig = 0.001
    zw = np.zeros(len(t))
    zw[gpd] = (R/t_d * np.exp(-(t[gpd]-tsk)/t_d) + R2/(sig*np.sqrt(2*np.pi))*np.exp(-((t[gpd]-tsk)**2/(2*sig**2)))) / (R+R2+R3)
    zw[gpd2] = (R3/t_r * np.exp((t[gpd2]-tsk)/t_r) + R2/(sig*np.sqrt(2*np.pi))*np.exp(-((t[gpd2]-tsk)**2/(2*sig**2)))) / (R+R2+R3)
    
    sig_Gauss = 0.043
    G = lambda x, p: p[0]*np.exp(-((x-p[1])**2)/(2*(p[2]**2)))
    yIRF = np.convolve(G(xIRF, [(1/(sig_Gauss*np.sqrt(2*np.pi))), 2, sig_Gauss]), zw, mode='same')
    yIRF = yIRF / (np.sum(yIRF) * t_binWidth)


#######################################################################
#######################################################################
#######################################################################
#######################################################################
# PART 4: --- Plot the pulse, semilogy ################################
#######################################################################
#######################################################################
#######################################################################
#######################################################################

plt.figure()
bin_centers = DecayBins[:-1] + np.diff(DecayBins) / 2
plt.semilogy(bin_centers, DecayCounts)
plt.title("SemiLogY of the pulse")
plt.grid(True)
plt.xlabel("Time (s)")
plt.ylabel("Counts")
plt.show()

plt.figure()
plt.plot(xIRF, yIRF)
plt.title("Impulse Response Function (IRF)")
plt.grid(True)
plt.xlabel("Time (s)")
plt.ylabel("Amplitude")
plt.show()

xtt = xIRF[yIRF > max(yIRF) / 2]
IRF_FWHM = (xtt[-1] - xtt[0]) * 1000
print(f"IRF FWHM [ps]: {IRF_FWHM:.2f}")

# --- Get the bins from histogram instead of the variable which I used to create the histogram. Super. ---
# DecayBins[DecayBins > 0][1] - DecayBins[DecayBins > 0][0]
# (DecayBins[DecayBins > 0][10] - DecayBins[DecayBins > 0][0]) / 10
# DecayBins[DecayBins > 0][3] - DecayBins[DecayBins > 0][2]
# DecayBins[DecayBins > 0][5] - DecayBins[DecayBins > 0][4]

binning = (DecayBins[DecayBins > 0][100] - DecayBins[DecayBins > 0][0]) / 100
# binning = t_binWidth * 1e9



##################################################################
##################################################################
##################################################################
##################################################################
# PART5: --- Compute and subtract baseline --- ###################
##################################################################
##################################################################
##################################################################
##################################################################

gpd = np.where((DecayBins > t_min_baseline) & (DecayBins < t_max_baseline))[0]  # for baseline
cbaseline = np.mean(DecayCounts[gpd]) + 0.001  # Average of the bin content% re-normalization for the minimum baseline
cbaseline = cbaseline / binning  # Average of the density of events

################################################ 
cbaseline = cbaseline / basfac
################################################## 

xt = np.arange(0, 500, binning)  # Create time bins between 0 and 500

# Get the scintillation points
nP = iChma  # The parameters
yt = DCF2(nP, xt)  # Pass it to the fit function.
yt = yt / np.sum(yt)  # Normalise the fit datapoints
# # Get the IRF points
yIRFi = np.interp(np.arange(min(xIRF), max(xIRF), binning), xIRF, yIRF)  # Linear interpolation between min(xIRF) and max(IRF) with step binning
yIRFi = yIRFi / np.sum(yIRFi)  # Normalisation

# Get the fit function points
ct = np.convolve(yIRFi, yt)  # DO THE CONVOLUTION!
# ct=ct/sum(ct);
ctx = np.arange(0, binning * (len(ct) - 1) + binning, binning)
y1 = ct  # interp1(ctx,ct,xt);      # y1 is the new fit function

DecayCounts1 = (DecayCounts - cbaseline * binning)  # / binning; %DecayCounts1 are the datapoints without baseline, divided by binning to get the density!

plt.figure()
bin_centers = DecayBins[:-1] + np.diff(DecayBins) / 2
plt.semilogy(bin_centers, DecayCounts1 / (np.sum(DecayCounts1)))  # Plot normalised datapoints density
plt.semilogy(xt, yt / np.sum(yt), 'b')  # Plot the scintillation function
plt.semilogy(ctx, y1 / np.sum(y1), 'r')  # Plot the fit function
plt.title("Normalised datapoints, and starting scintillation and pulse")

decaybiningfac = 1  # If you want to use different binning for decay time

zw1 = np.where((DecayBins >= 0) & (DecayBins <= rangedec))[0]  # Find all the time bins above 0 and below the given rangedec, i.e. the fit range.

sDC = np.sum(DecayCounts1[zw1])  # -cbaseline)  # New normalisation
xhma = DecayBins[zw1]  # x in the fit range
yhma = DecayCounts1[zw1] / sDC  # y normalised in the fit range

Fitjump1 = 1000
Fitjump2 = 100
iChma1 = iChma.copy()
iChma2 = iChma.copy()
X1 = 0
X2 = 0

progress = 0
progress_old = -1

RUNS = RUNStot
X2_i = np.zeros(1)  # Array to monitor the chi square over time
sel_X2_i = np.zeros(1)  # Array to monitor the chi square selected over time
sel_X2_iter = np.zeros(1)  # Array to monitor the iterations of the chi square selected over time

if onlyVisualization:
    print("onlyVisualization == 1\n STOP NOW\n")
    exit()


###############################################################
###############################################################
###############################################################
###############################################################
#PART 6: MNIMISATION ##########################################
###############################################################
###############################################################
###############################################################
###############################################################

for i in range(RUNS):
    progress = i // 1000          # Print every 1k runs
    if progress_old != progress:
        print(f"Iteration: {i}")
        progress_old = progress

    # --- Change the parameters to see what happens
    iChma2 = UpdateParameters(iChma1, iFitIt, i, RUNS)

    yhma1 = yhma
    yhma2 = yhma

    # Get the y values corresponding to parameters iChma1
    xt = xhma
    yt = DCF2(iChma1, xhma)
    yIRFi = np.interp(np.arange(min(xIRF), max(xIRF), binning),xIRF, yIRF)
    ct = np.convolve(yIRFi, yt)
    ct = ct / np.sum(ct)
    # ctx = np.arange(0, binning * len(ct), binning)
    ctx = np.arange(0, binning * (len(ct) - 1) + binning, binning)
    # print(ct.shape)
    # print(ctx.shape)
    y1 = np.interp(xt,ctx, ct)
    y1 = y1 / np.sum(y1)

    # Get the y values corresponding to parameters iChma2
    xt = xhma
    yt = DCF2(iChma2, xhma)
    yIRFi = np.interp(np.arange(min(xIRF), max(xIRF), binning),xIRF, yIRF)
    ct = np.convolve(yIRFi, yt)
    ct = ct / np.sum(ct)
    ctx = np.arange(0, binning * (len(ct) - 1) + binning, binning)
    y2 = np.interp(xt, ctx, ct)
    y2 = y2 / np.sum(y2)

    # Calculate the chi2 as (...)/sqrt(sigma) --> This is to weigh more the
    # scintillation peak!
    # Just unsure about re-adding the baseline.
    zws = np.where((y2 * sDC + cbaseline * binning) != 0)[0]           # Find the bins with something in
    Nbinzw = y2[zws] * sDC + cbaseline * binning
    Nzw = np.sum(Nbinzw)
    chi2 = np.sum((yhma[zws] * sDC - y2[zws] * sDC) ** 2 / np.sqrt(Nbinzw * (1 - Nbinzw / Nzw))) / len(zws)

    zws = np.where((y1 * sDC + cbaseline * binning) != 0)[0]
    Nbinzw = y1[zws] * sDC + cbaseline * binning
    Nzw = np.sum(Nbinzw)
    chi1 = np.sum((yhma[zws] * sDC - y1[zws] * sDC) ** 2 / np.sqrt(Nbinzw * (1 - Nbinzw / Nzw))) / len(zws)

    X2_i = np.append(X2_i , chi2)

    # anp = 1e-2 * (1 - i / RUNS)  # Growth rate for the logistic function (see later)
    anp = 0.001 * np.exp(-i / RUNS * 50)

    if np.random.rand() <= 1 / (1 + np.exp((chi2 - chi1) / anp)):
        chigutdecay = chi2
        chibaddecay = chi1

        zws = np.where((y2 * sDC + cbaseline * binning) != 0)[0]
        Nbinzw = y2[zws] * sDC + cbaseline * binning
        Nzw = np.sum(Nbinzw)
        chigut = np.sum((yhma[zws] * sDC - y2[zws] * sDC) ** 2 / (Nbinzw * (1 - Nbinzw / Nzw))) / len(zws)
        chierr = np.sqrt(2 / len(zws)) * 3

        iChma1 = iChma2
        sel_X2_i = np.append(sel_X2_i , chigutdecay) 
        sel_X2_iter = np.append(sel_X2_iter , i)
    else:
        pass


nPhma = iChma1.copy()

DecayCounts = DecayCounts - cbaseline * binning * decaybiningfac
sDC = np.sum(DecayCounts[zw1])
DecayCounts = DecayCounts / sDC
xhma = DecayBins[zw1]
yhma = DecayCounts[zw1]

# nPhma, cov = curve_fit(DCF2, xhma, yhma, iChma1, maxfev=100000, ftol=1e-15, xtol=1e-15)
# falltime = nPhma[1]
# fallabun = nPhma[2]
# fallabun = 0
# falltime2 = nPhma[3]
# IRFskewquess = nPhma[0]
# cbaselinefall = nPhma[3] * sDC
# cbaselinefall = nPhma[3] / binning / 100
cbaselinefall = cbaseline

x = xhma  # 112:binning*100:1400

xt = x
yt = DCF2(nPhma, xhma)
yIRFi = np.interp( np.arange(min(xIRF), max(xIRF), binning), xIRF, yIRF)
ct = np.convolve(yIRFi, yt)
ct = ct / np.sum(ct)
ctx = np.arange(0, binning * (len(ct)-1) + binning, binning)
y = np.interp( xt, ctx, ct)
y = y / np.sum(y)

plt.figure()
plt.plot(x, y)
plt.show()




#############################################################
#############################################################
#############################################################
#############################################################
#PART 7 : CHI2 plot##########################################
#############################################################
#############################################################
#############################################################
#############################################################

plt.figure()
iterations = np.arange(1, len(X2_i)+1)
print(X2_i)
plt.semilogy(iterations, X2_i)
plt.grid(True)
plt.scatter(sel_X2_iter, sel_X2_i, color='red', marker='o')
min_X2 = np.min(X2_i)
min_X2_i = np.argmin(X2_i)
plt.scatter(min_X2_i+1, min_X2, color='green', marker='*', s=100)
plt.ylabel('Chi2')
plt.xlabel('Iteration')
plt.title('Chi Square vs # Iteration')
plt.savefig(plotPrefix + '_Chi2Monitor.jpg', dpi=300)
plt.savefig(plotPrefix + '_Chi2Monitor.pdf', dpi=300)
plt.show()


# import sys
# sys.exit()






#####################################################################
#####################################################################
#####################################################################
#####################################################################
#####################################################################
#################### P L O T ########################################
#####################################################################
#####################################################################
#####################################################################
#####################################################################
#####################################################################
# --- P R I N T I N G ---
# -- Normalisation of the abundancies
textDecay1 = r'$\tau_{d_1}=$' + f'{nPhma[4]:6.3f}' + r'ns   (' + f'{(nPhma[1]/(nPhma[1]+nPhma[2]+nPhma[3]+nPhma[10]))*100:4.2f}' + r'% )'
textDecay2 = r'$\tau_{d_2}=$' + f'{nPhma[5]:6.3f}' + r'ns   (' + f'{(nPhma[2]/(nPhma[1]+nPhma[2]+nPhma[3]+nPhma[10]))*100:4.2f}' + r'% )'
textDecay3 = r'$\tau_{d_3}=$' + f'{nPhma[6]:6.3f}' + r'ns   (' + f'{(nPhma[3]/(nPhma[1]+nPhma[2]+nPhma[3]+nPhma[10]))*100:4.2f}' + r'% )'
textCAmp = r'$C_{amp}=$' + f'{(nPhma[10]/(nPhma[1]+nPhma[2]+nPhma[3]+nPhma[10]))*100:4.2f}' + r'%'



# -- Rise times
textRise1 = r'$\tau_{r_1}=$' + f'{nPhma[7]*1000:5.0f}' + 'ps'
textRise2 = r'$\tau_{r_2}=$' + f'{nPhma[8]*1000:5.0f}' + 'ps'
textRise3 = r'$\tau_{r_3}=$' + f'{nPhma[9]*1000:5.0f}' + 'ps'



#####################################################
#####################################################
#####################################################
## DECAY TIME PLOT
# rangedec = 50
# plt.figure()
fig, axs = plt.subplots(2, 1, figsize=(8, 8))


########################axis0
axs[0].plot(x, np.log10(y*sDC+cbaseline*binning*decaybiningfac), color='red', linewidth=2) #1.3
axs[0].scatter(xhma, np.log10(yhma*sDC+cbaseline*binning*decaybiningfac), s=10, color='blue', marker='o', alpha=0.5) #DATA POINTS
axs[0].plot(xhma, np.log10(smooth(yhma*sDC+cbaseline*binning*decaybiningfac, 20)), color='green', linewidth=2) #RUNNING AVERAGE
axs[0].plot(x, np.log10(y*sDC+cbaseline*binning*decaybiningfac), color='red', linewidth=1.3) #RUNNING AVERAGE

axs[0].set_title(plottitle + '  ' + f'{sum(yhma*sDC):6.0f}' + ' counts')
axs[0].set_ylabel('log10(Number of Counts)')
axs[0].set_ylim([np.log10(cbaseline*binning*decaybiningfac/2), np.log10(max(yhma*sDC+cbaseline*binning*decaybiningfac))*21/20])
axs[0].set_xlim([0, rangedec])
axs[0].grid(True)
axs[0].tick_params(axis='both', which='major', labelsize=15)
axs[0].set_xlabel(r'$\Delta T$ [ns]')

testz = np.zeros(len(y))
for iz in range(len(y)):
    if y[iz] * sDC + cbaseline * binning * decaybiningfac != 0:
        testz[iz] = (yhma[iz] - y[iz]) * sDC
    else:
        testz[iz] = (yhma[iz] - y[iz]) * sDC
axs[1].plot(xhma, testz, color='blue')
axs[1].plot(xhma, smooth(testz, 20), color='red', linewidth=1.3)
axs[1].plot(axs[1].get_xlim(), [0, 0], color='black', linewidth=1)

axs[1].set_ylabel('Res. [N. of C.]')
axs[1].set_xlabel(r'$\Delta T$ [ns]')
axs[1].tick_params(axis='both', which='major', labelsize=15)
axs[1].grid(True)

axs[0].annotate(textDecay1, xy=(0.6, 0.78), xycoords='figure fraction', fontsize=15, bbox=dict(alpha = 0, boxstyle='round', fc='w', ec='none'))
axs[0].annotate(textDecay2, xy=(0.6, 0.72), xycoords='figure fraction', fontsize=15, bbox=dict(alpha = 0, boxstyle='round', fc='w', ec='none'))
axs[0].annotate(textDecay3, xy=(0.6, 0.66), xycoords='figure fraction', fontsize=15, bbox=dict(alpha = 0, boxstyle='round', fc='w', ec='none'))


plt.subplots_adjust(hspace=0.5)

# plt.pause(5)

# plotPrefix is the prefix for the output file name
# this saves the current figure as a JPEG and PDF file
plt.savefig(plotPrefix + '_plot_SingleDecay_res_Decay.jpg', dpi=300, bbox_inches='tight')
plt.savefig(plotPrefix + '_plot_SingleDecay_res_Decay.pdf', dpi=300, bbox_inches='tight')
plt.show()

##########################################################
##########################################################
##########################################################
# RISE timeplot

import pandas as pd


fig, axs = plt.subplots(2, 1, figsize=(8, 8))

axs[0].plot(     x,                             y*sDC+cbaseline*binning*decaybiningfac, color='red', linewidth=2)
axs[0].scatter(  xhma,                          yhma*sDC+cbaseline*binning*decaybiningfac, s=10, color='blue', alpha=0.5)
axs[0].plot(     xhma,                          smooth(yhma*sDC+cbaseline*binning*decaybiningfac), color='green', linewidth=2)
axs[0].plot(     x,                             y*sDC+cbaseline*binning*decaybiningfac, color='green', linewidth=2)
axs[0].scatter(  xhma,                          yhma*sDC+cbaseline*binning*decaybiningfac, s=10, color='blue', marker='o')
axs[0].plot(     xIRF-min(xIRF)+nPhma[0]+0.013, yIRF/np.max(yIRF)*np.max(y*sDC)+cbaseline*binning*decaybiningfac, linestyle=':', color=[0.4, 0.4, 0.4], linewidth=1.5)

axs[0].grid(True)
axs[0].set_title(f"{plottitle} {sum(yhma*sDC):.0f} counts")
axs[0].set_ylabel("Number of Counts")
axs[0].set_xlim(rangerise[0], rangerise[1])
axs[0].set_ylim(0, (max(yhma*sDC+cbaseline*binning*decaybiningfac))*21/20)
axs[0].set_xlabel(r'$\Delta T$ [ns]')


testz = np.zeros(len(y))
for iz in range(len(y)):
    if y[iz] * sDC + cbaseline * binning * decaybiningfac != 0:
        testz[iz] = (yhma[iz] - y[iz]) * sDC
    else:
        testz[iz] = (yhma[iz] - y[iz]) * sDC
axs[1].plot(xhma, testz, color='blue')
axs[1].plot(xhma, smooth(testz, 20), color='red', linewidth=1.3)
axs[1].plot(axs[1].get_xlim(), [0, 0], color='black', linewidth=1)

axs[1].set_ylabel('Res. [N. of C.]')
axs[1].set_xlabel(r'$\Delta T$ [ns]')
axs[1].tick_params(axis='both', which='major', labelsize=15)
axs[1].grid(True)
axs[1].set_xlim(rangerise[0], rangerise[1])
axs[1].set_ylim(min((yhma-y)*sDC), max((yhma-y)*sDC))

axs[1].annotate(textDecay1, xy=(0.6, 0.37), xycoords='figure fraction', fontsize=15, bbox=dict(alpha = 0, boxstyle='round', fc='w', ec='none') )
axs[1].annotate(textDecay2, xy=(0.6, 0.31), xycoords='figure fraction', fontsize=15, bbox=dict(alpha = 0, boxstyle='round', fc='w', ec='none') )
axs[1].annotate(textDecay3, xy=(0.6, 0.26), xycoords='figure fraction', fontsize=15, bbox=dict(alpha = 0, boxstyle='round', fc='w', ec='none') )
axs[1].annotate(textRise1,  xy=(0.6, 0.20), xycoords='figure fraction', fontsize=15, bbox=dict(alpha = 0, boxstyle='round', fc='w', ec='none') )


# plt.subplots_adjust(hspace=0.5)


# plt.pause(5)

plt.savefig(plotPrefix + '_plot_SingleDecay_res_Rise.jpg', dpi=300, bbox_inches='tight')
plt.savefig(plotPrefix + '_plot_SingleDecay_res_Rise.pdf', dpi=300, bbox_inches='tight')
plt.show()
# SAVE the datapoints to TXT
xplot = x
yplot = (yhma*sDC+cbaseline*binning*decaybiningfac)

# Create a dataframe with the data
df = pd.DataFrame({'xplot': xplot, 'yplot': yplot})

# Write data to text file
df.to_csv(f"{plotPrefix}_graphsDataPoints.txt", sep=' ', index=False)



##########################################################
##########################################################
##########################################################
# Log10 RISE timeplot


fig, axs = plt.subplots(2, 1, figsize=(8, 8))

axs[0].plot(     x,                             np.log10(y*sDC+cbaseline*binning*decaybiningfac), color='red', linewidth=2)
axs[0].scatter(  xhma,                          np.log10(yhma*sDC+cbaseline*binning*decaybiningfac), s=10, color='blue', alpha=0.5)
axs[0].plot(     xhma,                          np.log10(smooth(yhma*sDC+cbaseline*binning*decaybiningfac)), color='green', linewidth=2)
axs[0].plot(     x,                             np.log10(y*sDC+cbaseline*binning*decaybiningfac), color='green', linewidth=2)
axs[0].scatter(  xhma,                          np.log10(yhma*sDC+cbaseline*binning*decaybiningfac), s=10, color='blue', marker='o')
axs[0].plot(     xIRF-min(xIRF)+nPhma[0]+0.013, np.log10(yIRF/np.max(yIRF)*np.max(y*sDC)+cbaseline*binning*decaybiningfac), linestyle=':', color=[0.4, 0.4, 0.4], linewidth=1.5)
axs[0].grid(True)
axs[0].set_title(f"{plottitle} {sum(yhma*sDC):.0f} counts")
axs[0].set_ylabel("log10(Number of Counts)")
axs[0].set_xlim(rangerise[0], rangerise[1])
axs[0].set_ylim(0, log10(max(yhma*sDC+cbaseline*binning*decaybiningfac))*21/20)

testz = np.zeros(len(y))
for iz in range(len(y)):
    if y[iz] * sDC + cbaseline * binning * decaybiningfac != 0:
        testz[iz] = (yhma[iz] - y[iz]) * sDC
    else:
        testz[iz] = (yhma[iz] - y[iz]) * sDC
axs[1].plot(xhma, testz, color='blue')
axs[1].plot(xhma, smooth(testz, 20), color='red', linewidth=1.3)
axs[1].plot(axs[1].get_xlim(), [0, 0], color='black', linewidth=1)
axs[1].set_ylabel('Res. [N. of C.]')
axs[1].set_xlabel(r'$\Delta T$ [ns]')
axs[1].tick_params(axis='both', which='major', labelsize=15)
axs[1].grid(True)
axs[1].set_xlim(rangerise[0], rangerise[1])
axs[1].set_ylim(min((yhma-y)*sDC), max((yhma-y)*sDC))

axs[1].annotate(textDecay1, xy=(0.6, 0.37), xycoords='figure fraction', fontsize=15, bbox=dict(alpha = 0, boxstyle='round', fc='w', ec='none') )
axs[1].annotate(textDecay2, xy=(0.6, 0.31), xycoords='figure fraction', fontsize=15, bbox=dict(alpha = 0, boxstyle='round', fc='w', ec='none') )
axs[1].annotate(textDecay3, xy=(0.6, 0.26), xycoords='figure fraction', fontsize=15, bbox=dict(alpha = 0, boxstyle='round', fc='w', ec='none') )
axs[1].annotate(textRise1,  xy=(0.6, 0.20), xycoords='figure fraction', fontsize=15, bbox=dict(alpha = 0, boxstyle='round', fc='w', ec='none') )


plt.subplots_adjust(hspace=0.5)
plt.show()


# plt.pause(5)

plt.savefig(plotPrefix + '_plot_SingleDecay_res_Riselog10.jpg', dpi=300, bbox_inches='tight')
plt.savefig(plotPrefix + '_plot_SingleDecay_res_Riselog10.pdf', dpi=300, bbox_inches='tight')


print('The effective decay time is: {:.2f} ns'.format(1 / (1/nPhma[4]*(nPhma[1]/(nPhma[1]+nPhma[2]+nPhma[3])) + 1/nPhma[5]*(nPhma[2]/(nPhma[1]+nPhma[2]+nPhma[3])) + 1/nPhma[6]*(nPhma[3]/(nPhma[1]+nPhma[2]+nPhma[3])))))

print('minimum chi2 is: {:.2f}'.format(min(X2_i)))
