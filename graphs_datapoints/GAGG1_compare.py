# from matplotlib import pyplot as plt
import numpy as np
# import mplhep 
# plt.style.use(mplhep.style.ROOT)
import ROOT as rt

rt.gROOT.SetBatch(1)
rt.gStyle.SetOptStat(0)
rt.gROOT.ProcessLine(".x /Users/zhshen/lhcbLib/lhcbStyle.C")

def read_file(filename):
    T = [] #Y axis 
    Wavelength = [] # x axis
    start_flag = False
    for line in open(filename, encoding='utf-8'):
        # print(line)
        if "xplot" in line:
            start_flag=True
            continue
        if start_flag==True:    
            line_sp = line.split()
            if len(line_sp)<2:
                continue
            # if float(line_sp[0])>500:
            #     continue
            # print("x: ", line)
            T.append( float(line_sp[1]) )
            Wavelength.append( float(line_sp[0]) )
    return [Wavelength, T]

datas = []
top1 = read_file("./h5_4474_420longpass_500kHz_graphsDataPoints.txt")
datas.append(top1)
top2 = read_file("./h7_4475_420longpass_500kHz_graphsDataPoints.txt")
datas.append(top2)
middle1 = read_file("./M1_4476_420longpass_500kHz_graphsDataPoints.txt")
datas.append(middle1)
middle2 = read_file("./m3_4477_420longpass_500kHz_graphsDataPoints.txt")
datas.append(middle2)
tail1 = read_file("./T6_4478_420longpass_500kHz_graphsDataPoints.txt")
datas.append(tail1)
tail2 = read_file("./T7_4479_420longpass_500kHz_graphsDataPoints.txt")
datas.append(tail2)

npdatas = []
for i in range(6):
    npdatas.append(np.asarray(datas[i][1]))
    mean = np.mean(npdatas[i][:100])
    npdatas[i] -= mean
    npdatas[i] /= np.max(npdatas[i])


# fill the hist
n_bins = 600
x_min = 0
x_max = 600
hists = []
colors = [1,2,4,6,8,9] 
for i in range(6):
    h1 = rt.TH1D("h"+str(i), "", n_bins, x_min, x_max)
    for j in range(len(datas[i][0])):
        h1.Fill(datas[i][0][j], npdatas[i][j])
    hists.append(h1)
    hists[i].SetLineColor(colors[i])
    hists[i].SetLineWidth(1)

c = rt.TCanvas()
hists[0].Draw("hist L")
hists[0].SetMaximum(1.5*hists[0].GetMaximum())
hists[0].GetXaxis().SetTitle("#Delta T [ns]")
hists[0].GetYaxis().SetTitle("A.U.")
hists[0].Draw("hist L")
hists[1].Draw("hist L same")
hists[2].Draw("hist L same")
hists[3].Draw("hist L same")
hists[4].Draw("hist L same")
hists[5].Draw("hist L same")
# c.Draw()
legend = rt.TLegend(0.7, 0.7, 0.9, 0.9)
legend.AddEntry(hists[0], "Top5     #4474", "l")
legend.AddEntry(hists[1], "Top7      #4475", "l")
legend.AddEntry(hists[2], "Middle1   #4476", "l")
legend.AddEntry(hists[3], "Middle3   #4477", "l")
legend.AddEntry(hists[4], "Tail6     #4478", "l")
legend.AddEntry(hists[5], "Tail7     #4479", "l")
legend.Draw()
c.SetLogy(1)
c.SaveAs("./GAGG1_compare.pdf")
