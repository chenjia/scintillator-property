function params = UpdateParameters(params, paramsFitted, i, iMax)

% --- Theta ---
if paramsFitted(1)
    params(1)    = params(1) + (1-2*rand(1)) * 0.1 * (iMax-i)/iMax;  % Cool, basically move the theta but less and less over time.
end

% --- Amplitudes ---
if paramsFitted(2)
    params(2)    = params(2) + (1-2*rand(1))*(0.005+params(2)*0.1)*(iMax-i)/iMax;    % Amplitude first decay time
end
if paramsFitted(3)
    params(3)    = params(3) + (1-2*rand(1))*(0.005+params(3)*0.1)*(iMax-i)/iMax;    % Amplitude second decay time
end
if paramsFitted(4)
    params(4)    = params(4) + (1-2*rand(1))*(0.005+params(4)*0.1) *(iMax-i)/iMax;    % Amplitude third decay time
end

% --- Tau Decay ---
if paramsFitted(5)
    params(5)    = params(5)+(1-2*rand(1))*(0.1*params(5)+0.01)*(iMax-i)/iMax;  % First decay time
end
if paramsFitted(6)
    params(6)    = params(6)+(1-2*rand(1))*(0.1*params(6)+0.01)*(iMax-i)/iMax;  % Second decay time
end
if paramsFitted(7)
    params(7)    = params(7)+(1-2*rand(1))*(0.1*params(7)+0.01)*(iMax-i)/iMax;  % Third decay time
end



% --- Tau Rise ---
if paramsFitted(8)
    params(8)    = params(8) +(1-2*rand(1))*0.01 *(iMax-i)/iMax;  % First rise time
end
if paramsFitted(9)
    params(9)    = params(9) +(1-2*rand(1))*0.01 *(iMax-i)/iMax;  % Second rise time
end
if paramsFitted(10)
    params(10)   = params(10) +(1-2*rand(1))*0.01 *(iMax-i)/iMax;  % Third rise time
end

% --- C_Amp ---
if paramsFitted(11)
    params(11)   = params(11) + (1-2*rand(1))*(0.005+params(11)*0.01)*(iMax-i)/iMax;    % Amplitude first decay time
end

% --- Make sure all values are non-negative
params( params < 0.) = 0.;

end
