import numpy as np

def UpdateParameters(params, paramsFitted, i, iMax):
    
    # --- Theta ---
    if paramsFitted[0]:
        params[0] = params[0] + (1-2*np.random.rand()) * 0.1 * (iMax-i)/iMax  # Cool, basically move the theta but less and less over time.
    
    # --- Amplitudes ---
    if paramsFitted[1]:
        params[1] = params[1] + (1-2*np.random.rand())*(0.005+params[1]*0.1)*(iMax-i)/iMax  # Amplitude first decay time
    if paramsFitted[2]:
        params[2] = params[2] + (1-2*np.random.rand())*(0.005+params[2]*0.1)*(iMax-i)/iMax  # Amplitude second decay time
    if paramsFitted[3]:
        params[3] = params[3] + (1-2*np.random.rand())*(0.005+params[3]*0.1)*(iMax-i)/iMax  # Amplitude third decay time
    
    # --- Tau Decay ---
    if paramsFitted[4]:
        params[4] = params[4] + (1-2*np.random.rand())*(0.1*params[4]+0.01)*(iMax-i)/iMax  # First decay time
    if paramsFitted[5]:
        params[5] = params[5] + (1-2*np.random.rand())*(0.1*params[5]+0.01)*(iMax-i)/iMax  # Second decay time
    if paramsFitted[6]:
        params[6] = params[6] + (1-2*np.random.rand())*(0.1*params[6]+0.01)*(iMax-i)/iMax  # Third decay time
    
    # --- Tau Rise ---
    if paramsFitted[7]:
        params[7] = params[7] + (1-2*np.random.rand())*0.01 *(iMax-i)/iMax  # First rise time
    if paramsFitted[8]:
        params[8] = params[8] + (1-2*np.random.rand())*0.01 *(iMax-i)/iMax  # Second rise time
    if paramsFitted[9]:
        params[9] = params[9] + (1-2*np.random.rand())*0.01 *(iMax-i)/iMax  # Third rise time
    
    # --- C_Amp ---
    if paramsFitted[10]:
        params[10] = params[10] + (1-2*np.random.rand())*(0.005+params[10]*0.01)*(iMax-i)/iMax  # Amplitude first decay time
    
    # --- Make sure all values are non-negative
    params[params < 0.] = 0.
    
    return params
