function output= gauss (param,t)

% common part
tsk  = param(1);
R    = param(2);
R2   = param(3);
R3   = param(4);
t_d  = param(5);
t_d2 = param(6);
t_d3 = param(7);
t_r  = param(8);
t_r2 = param(9);
t_r3 = param(10);
CAmp = param(11);

% Prompt photons part
tskpr=0;
sigma_min = 0.01;  %A sigma as small as possible for the prompt photons. Smaller than a bin

% % common part
  gpd=find(t>tsk);
% 
% --- 1 tau_rise, 3 tau_decay
     zw(gpd)= (R/(t_d-t_r).*(exp(-(t(gpd)-tsk)/t_d)-exp(-(t(gpd)-tsk)/t_r))  +  R2/(t_d2-t_r).*(exp(-(t(gpd)-tsk)/t_d2)-exp(-(t(gpd)-tsk)/t_r))  +  R3/(t_d3-t_r).*(exp(-(t(gpd)-tsk)/t_d3)-exp(-(t(gpd)-tsk)/t_r)))  /  (R+R2+R3);

% --- 2 tau_rise, 2 tau_decay
%    zw(gpd)= (R/(t_d-t_r).*(exp(-(t(gpd)-tsk)/t_d)-exp(-(t(gpd)-tsk)/t_r))  +  R2/(t_d2-t_r2).*(exp(-(t(gpd)-tsk)/t_d2)-exp(-(t(gpd)-tsk)/t_r2)))  /  (R+R2);

% --- 2 tau_rise, 3 tau_decay
%     zw(gpd)= (R/(t_d-t_r).*(exp(-(t(gpd)-tsk)/t_d)-exp(-(t(gpd)-tsk)/t_r))  +  R2/(t_d2-t_r2).*(exp(-(t(gpd)-tsk)/t_d2)-exp(-(t(gpd)-tsk)/t_r2))  +  R3/(t_d3-t_r).*(exp(-(t(gpd)-tsk)/t_d3)-exp(-(t(gpd)-tsk)/t_r)))  /  (R+R2+R3);

% --- 3 tau_rise, 3 tau_decay
%    zw(gpd)= (R/(t_d-t_r).*(exp(-(t(gpd)-tsk)/t_d)-exp(-(t(gpd)-tsk)/t_r))  +  R2/(t_d2-t_r2).*(exp(-(t(gpd)-tsk)/t_d2)-exp(-(t(gpd)-tsk)/t_r2))  +  R3/(t_d3-t_r3).*(exp(-(t(gpd)-tsk)/t_d3)-exp(-(t(gpd)-tsk)/t_r3)))  /  (R+R2+R3);

% --- 1 tau_rise, 2 tau_decay + C_amp
%    zw(gpd)= (CAmp / (sigma_min*sqrt(2*3.1415)).*exp( - (((t(gpd)-tsk).^2)/(2*sigma_min.^2)) ) + (R/(t_d-t_r).*(exp(-(t(gpd)-tsk)/t_d)-exp(-(t(gpd)-tsk)/t_r))  +  R2/(t_d2-t_r).*(exp(-(t(gpd)-tsk)/t_d2)-exp(-(t(gpd)-tsk)/t_r))))  /  (R+R2+CAmp);

% --- 2 tau_rise, 2 tau_decay + C_amp
%    zw(gpd)= (CAmp / (sigma_min*sqrt(2*3.1415)).*exp( - (((t(gpd)-tsk).^2)/(2*sigma_min.^2)) ) + (R/(t_d-t_r).*(exp(-(t(gpd)-tsk)/t_d)-exp(-(t(gpd)-tsk)/t_r))  +  R2/(t_d2-t_r2).*(exp(-(t(gpd)-tsk)/t_d2)-exp(-(t(gpd)-tsk)/t_r2))))  /  (R+R2+CAmp);

% --- 1 tau_rise, 3 tau_decay + C_amp
%    zw(gpd)= (CAmp / (sigma_min*sqrt(2*3.1415)).*exp( - (((t(gpd)-tsk).^2)/(2*sigma_min.^2)) ) + (R/(t_d-t_r).*(exp(-(t(gpd)-tsk)/t_d)-exp(-(t(gpd)-tsk)/t_r))  +  R2/(t_d2-t_r).*(exp(-(t(gpd)-tsk)/t_d2)-exp(-(t(gpd)-tsk)/t_r))  +  R3/(t_d3-t_r).*(exp(-(t(gpd)-tsk)/t_d3)-exp(-(t(gpd)-tsk)/t_r))))  /  (R+R2+R3+CAmp);






output=zw/sum(zw);


end


%%% Ma che � 'sta roba? La spazzatura a bordo strada sulla Varesina.
%%%
% dt = param(1);
% tau = param(2);
% R=param(3);
% tau2 = param(4);
% s=param(5);
% fsd=param(8);
% CAmp=param(7);
% 
% CAmp=0;
% % zw= 1/tau*exp(-(t-dt)/tau).*heaviside(t-dt);
% 
% if fsd==1
%     tau2=tau;
%     zw= 1/tau*1/2*exp((s^2-2*t*tau+2*tau*dt)/(2*tau^2)).*(1-erf((s^2+tau*(dt-t))/(sqrt(2)*s*tau)))+CAmp/(sqrt(2*pi).*(s))*exp(-(t-dt).^2/(2*(s).^2))+CAmp/(sqrt(2*pi).*(s))*exp(-(t-dt).^2/(2*(s).^2));%c*R*exp(-c*1e9*(t-dt));
% % % %     zw= 1/tau*exp(-(t-dt)/tau).*heaviside(t-dt);
% elseif fsd==2
%     zw= (1-R)/tau*1/2*exp((s^2-2*t*tau+2*tau*dt)/(2*tau^2)).*(1-erf((s^2+tau*(dt-t))/(sqrt(2)*s*tau)))+R/tau2*1/2*exp((s^2-2*t*tau2+2*tau2*dt)/(2*tau2^2)).*(1-erf((s^2+tau2*(dt-t))/(sqrt(2)*s*tau2)))+CAmp/(sqrt(2*pi).*(s))*exp(-(t-dt).^2/(2*(s).^2));
% end
% % % % zw= (1-R)/tau*1/2*exp((s^2-2*t*tau+2*tau*dt)/(2*tau^2)).*(1-erf((s^2+tau*(dt-t))/(sqrt(2)*s*tau)))+R/tau*1/2*exp((s^2-2*t*tau+2*tau*tau2)/(2*tau^2)).*(1-erf((s^2+tau*(tau2-t))/(sqrt(2)*s*tau)));
% 
% % % % zw= (1-R)/tau*1/2*exp((s^2-2*t*tau+2*tau*dt)/(2*tau^2)).*(1-erf((s^2+tau*(dt-t))/(sqrt(2)*s*tau)))+R*1/tau2*exp(-(t)/tau2).*heaviside(t);
% output=zw/sum(zw); 



% digits(50);
% output = R*exp(-(t-tsk)/t_d).*(1-exp(-(t-tsk)/t_r)).*heaviside(t-tsk);

% % % if t_r>0.009 && t_r2>0.009
% % %     zw= (1-R)/(t_d-t_r)*(1/2*exp((s^2-2*t*t_d+2*t_d*tsk)/(2*t_d^2)).*(1-erf((s^2+t_d*(tsk-t))/(sqrt(2)*s*t_d)))- ...
% % %         1/2*exp((s^2-2*t*t_r+2*t_r*tsk)/(2*t_r^2)).*(1-erf((s^2+t_r*(tsk-t))/(sqrt(2)*s*t_r)))) + ...
% % %         R/(t_d2-t_r2)*(1/2*exp((s^2-2*t*t_d2+2*t_d2*tsk)/(2*t_d2^2)).*(1-erf((s^2+t_d2*(tsk-t))/(sqrt(2)*s*t_d2)))- ...  
% % %         1/2*exp((s^2-2*t*t_r2+2*t_r2*tsk)/(2*t_r2^2)).*(1-erf((s^2+t_r2*(tsk-t))/(sqrt(2)*s*t_r2)))) + ...
% % %         CAmp/(sqrt(2*pi).*(s))*exp(-(t-tsk-tskpr).^2/(2*(s).^2));
% % %          
% % % % elseif t_r<=0.015 && t_r>=0
% % % %     zw= (1/(t_d)*(1/2*exp((s^2-2*t*t_d+2*t_d*tsk)/(2*t_d^2)).*(1-erf((s^2+t_d*(tsk-t))/(sqrt(2)*s*t_d)))));
% % % else 
% % % %     zw=0;
% % %     zw= (1-R)/(t_d-t_r)*(1/2*exp((s^2-2*t*t_d+2*t_d*tsk)/(2*t_d^2)).*(1-erf((s^2+t_d*(tsk-t))/(sqrt(2)*s*t_d)))+ ...
% % %         R/(t_d2-t_r2)*(1/2*exp((s^2-2*t*t_d2+2*t_d2*tsk)/(2*t_d2^2)).*(1-erf((s^2+t_d2*(tsk-t))/(sqrt(2)*s*t_d2))))) + ...
% % %         CAmp/(sqrt(2*pi).*(s))*exp(-(t-tsk-tskpr).^2/(2*(s).^2));
% % % end
     
% zw=(1-R)/(t_d-t_r).*(exp(-(t-tsk)/t_d)-exp(-(t-tsk)/t_r)).*heaviside(t-tsk)+R/(t_d2-t_r).*(exp(-(t-tsk)/t_d2)-exp(-(t-tsk)/t_r)).*heaviside(t-tsk);

% % % if t_r>0.01
% % % zw=(R/(t_d-t_r).*(exp(-(t-tsk)/t_d)-exp(-(t-tsk)/t_r)).*heaviside(t-tsk)+R2/(t_d2-t_r).*(exp(-(t-tsk)/t_d2)-exp(-(t-tsk)/t_r)).*heaviside(t-tsk)+R3/(t_d3-t_r).*(exp(-(t-tsk)/t_d3)-exp(-(t-tsk)/t_r)).*heaviside(t-tsk))/(R+R2+R3);
% % % else
% % % zw=(R/(t_d-t_r).*(exp(-(t-tsk)/t_d)).*heaviside(t-tsk)+R2/(t_d2-t_r).*(exp(-(t-tsk)/t_d2)).*heaviside(t-tsk)+R3/(t_d3-t_r).*(exp(-(t-tsk)/t_d3)).*heaviside(t-tsk))/(R+R2+R3);    
% % % end
% zw=1/(t_d-t_r).*(exp(-(t-tsk)/t_d)-exp(-(t-tsk)/t_r)).*heaviside(t-tsk);





% zw(gpd)=(R/(t_d).*(exp(-(t(gpd)-tsk)/t_d)-exp(-(t(gpd)-tsk)/t_r))+R2/(t_d2).*(exp(-(t(gpd)-tsk)/t_d2)-exp(-(t(gpd)-tsk)/t_r))+R3/(t_d3).*(exp(-(t(gpd)-tsk)/t_d3)-exp(-(t(gpd)-tsk)/t_r)))/(R+R2+R3);