import numpy as np
import matplotlib.pyplot as plt

# Create some data
x = np.array([1, 2, 3, 4])
y = np.array([2, 4, 1, 3])

fig, axs = plt.subplots(3, 1, figsize=(8, 8))

axs[0].plot(x,y)
axs[1].plot(x,y)
axs[2].plot(x,y)

plt.show()

b=1
a=3
a = 4 if b == 1 else a