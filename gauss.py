import numpy as np

def gauss(param, x):
    m = param[0]
    s = param[1]
    N = param[2]
    t_r = param[3]
    t_d = param[4]
    tsk = param[5]
    R = param[6]
    c = param[7]

    # hilf = x - m
    # output = m * hilf ** 3 + s * x ** 2 + N * x ** 1
    # this is the 3rd order polynomial equation here
    # output = 1 / (N * np.sqrt(2 * np.pi * s ** 2)) * np.exp(-(x - m) ** 2 / (2 * s ** 2)) + c

    # output = N * np.exp(-(x - m) ** 2 / (2 * s ** 2)) + R * (np.exp(-(x - (tsk)) / t_d) - np.exp(-(x - (tsk)) / t_r)) * np.heaviside(x - (tsk), 1) + c
    # output = N * np.exp(-(x - m) ** 2 / (2 * s ** 2)) + R * (np.exp(-(x - (m)) / t_d) - np.exp(-(x - (m)) / t_r)) * np.heaviside(x - (m), 1) + c
    output = N * np.exp(-(x - m) ** 2 / (2 * s ** 2)) + c
    # output = normpdf(x, m, s)
    return output
