
%%
%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%% REMEMBER TO Change lowECut and highECut
%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%
clear all
close all hidden;
clearvars -except iCit;

% if finish==0, clear all;close all;finish=0; end
% path1='/media/Work2/RiseTimeBench2013/ANALYSISES/';
% path1='/home/gund/Desktop/Analysis_RiseTime_high_stats_1stanalysis/';
% path='/media/THOR/Analysis_Rise_Time_HDO_IDQ_newSetup_oldStart56pF_2x2x5LSOCe04Ca_270220/';
%path='/Users/loris/Desktop/';
path='/home/roberto/Scrivania/February21/RiseAndDecayTime/';
%path='/Volumes/SSD_LHCb/511RnD/';

    %cry_ID='BSO_3892_4x5x6_IDQ_HPK6x6-60_Ch1high20mV_NINO60_050121';
    cry_ID='BSO_3892_4x5x6_IDQ_HPK6x6-60_UVto395nm-Ch1high20mV_NINO60_230121';
    %cry_ID='BSO_UVto395nm/BSO_3892_4x5x6_IDQ_HPK6x6-60_UVto395nm-Ch1high20mV_NINO60_230121';
    noKetek=0      % If no Ketek signal is expected, the program uses the start detector instead. Just a trick. Must be ignored afterwards.
    showPulses = 0; % if 1, show the pulses event by event.
    
Ch1Threshold = 10;         % sets the threshold on Ch1 signals  - default 30
Ch1IntegrationWindow = -1  % sets the time window used for Ch1 signal integration to get the energy spectrum.
                           % Use -1 if you want the integration to be performed on all the positive part of the signal.
                           % default 150
               



path0=[path cry_ID '/'];

finish=1; 
%%

if finish==1

    clear fnH50 fnH51 fnH52 fnH53;
    
    ii0=1; ii1=1; ii2=1; ii3=1;
    clear strs;
    a=dir(path0);
    for is=3:length(a)
     szw=[a(is).name];
     if strcmp(szw(1:2),'C1')
%     if strcmp(szw(1:2),'C4')
         fnH50{ii0}=[path0 a(is).name];
         ii0=ii0+1;
     end     
     if strcmp(szw(1:2),'C2')
         fnH51{ii1}=[path0 a(is).name];
         ii1=ii1+1;
     end
     if strcmp(szw(1:2),'C3')
         fnH52{ii2}=[path0 a(is).name];
         ii2=ii2+1;
     end
     if strcmp(szw(1:2),'C4')
         fnH53{ii3}=[path0 a(is).name];
         ii3=ii3+1;
     end
%      stop
    end

    
   if noKetek==1
    fnH50=fnH53;
   end
   
    
% [fnH51,path1] = uigetfile('C2*.trc;','MultiSelect','on');
% [fnH52,path2] = uigetfile('C3*.trc;','MultiSelect','on');
% [fnH53,path3] = uigetfile('C4*.trc;','MultiSelect','on');
  

%%%%%%%%%%%%%%%%%%%%%% Check if all the C# have the same length. Switch it
%%%%%%%%%%%%%%%%%%%%%% off if you do not have the energy of the tested
%%%%%%%%%%%%%%%%%%%%%% sample.
if length(fnH50)~=length(fnH51) || length(fnH51)~=length(fnH52) || length(fnH52)~=length(fnH53) || length(fnH51)~=length(fnH53)
    length(fnH50)
    length(fnH51)
    length(fnH52)
    length(fnH53)
     stop
end

tstr=char(fnH52(1))
number=[length(tstr)-4-4:length(tstr)-4]

%%
    %%% Check Waveform Data and initialize time array
    % I have this section here just to take a peak at the waveforms
clear t1
clear t

interval=0.01;

% Arraylength=398533;
Arraylength=length(fnH51);

Estart=zeros(Arraylength,1);
TCh2=zeros(Arraylength,1);
TCh3=zeros(Arraylength,1);
TCh4=zeros(Arraylength,1);
EnergyCh1=zeros(Arraylength,1);
SkewCh2=zeros(Arraylength,1);

% delete(gcp('nocreate'));
% parpool(4);

progress=0;progress_old=-1;

outtrop=0;
%%%%%%%%%%%%%%%%%%%%%%%%
for kk=1:Arraylength
%     if kk~=208898 && kk~=426924 %PbF2_Epic_3718_VUVSPAD40_NINO60_BLACK_260919_final
      if 1
    
        progress = floor(kk/Arraylength*100);
        if progress_old~=progress
            progress_old=progress;
            clc
            disp('|=================================================|');
            disp('|                 Opening files                   |');
            disp('|=================================================|');
            disp(['|================= ',num2str(progress),'% completed =================|']);
            disp(' ');        disp(['      Number of files to open: ',num2str(Arraylength)]);
        end
    

        clear Ch4 t4 Ch3 t3 Ch2 t2 Ch1 t1;
        
        clear waveform Ch4smoothed Ebin;
waveform=ReadLeCroyBinaryWaveform([fnH53{kk}]); 
Ch4=waveform.y(:)*1e3;
t4=waveform.x*1e9;

%%Ch4 is energy of start
%%Ch3 is NINO
%%Ch2 is SPAD

Ch4smoothed=smooth(Ch4,1000);
Estart(kk)=max(Ch4smoothed);

Ebin=find(Ch4(:)>100,1);

% if length(find(Ch4(:)>100))>1
%     stop
% end

if length(Ebin)~=0
    TCh4(kk)=t4(Ebin);

        clear waveform;      
    waveform=ReadLeCroyBinaryWaveform([fnH50{kk}]);    
    t1=waveform.x*1e9;
    Ch1=waveform.y(:)*(1e3);        
    Ch1smoothed=smooth(Ch1,1000);       
    Ch1smoothed(find(Ch1smoothed<0))=0;
    
        clear waveform;      
    waveform=ReadLeCroyBinaryWaveform([fnH51{kk}]);    
    t2=waveform.x*1e9;
    Ch2=waveform.y(:)*(-1e3);

        clear waveform;
    waveform=ReadLeCroyBinaryWaveform([fnH52{kk}]); 
    Ch3=waveform.y(:)*1e3;
    t3=waveform.x*1e9;
  
     lowEcut=525;        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%     highEcut=600;       %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%     lowEcut=360;        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%     highEcut=400;       %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%    lowEcut=510;        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    highEcut=590;       %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%     lowEcut=480;        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%     highEcut=530;       %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%     lowEcut=0;        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%     highEcut=2000;       %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  if Estart(kk)>lowEcut && Estart(kk)<highEcut %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 584 peak
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   
% %     maxt=t4(find(max(Ch4)==Ch4,1));
    maxt2=t2(find(min(Ch2)==Ch2,1));   
    vpks=sort(findpeaks(Ch4smoothed(find(Ch4smoothed>10))));
    

    if length(vpks)~=0
        if vpks(length(vpks))-vpks(1)<2
            pks=1;
        else
            pks=length(sort(findpeaks(Ch4smoothed(find(Ch4smoothed>0)))));
        end
    else
        pks=2;
    end
    
    
    
    % I BELIEVE HERE IS LOOKING FOR THE PEAK MAXIMA.
    try
        vpksCh1=sort(findpeaks(Ch1smoothed(find(Ch1smoothed>10))));    
    catch
        vpksCh1=0;
    end
    
    if vpksCh1~=0
        if vpksCh1(length(vpksCh1))-vpksCh1(1)<10
            pksCh1=1;
        else
            pksCh1=length(vpksCh1);
        end
    else
        pksCh1=0;
    end
        
    
           
%     Ch2smoothed=smooth(Ch2,100);
    pksc2=length(sort(findpeaks(-Ch2(find(-Ch2>100)))));
%     stop
%     if TCh4(kk)<maxt+10 && TCh4(kk)>maxt-40 && maxt2>-10 && maxt2<10 && pks<=1
    if maxt2>-5 && maxt2<15 && pks<=1 && pksCh1<=1
%     if pksc2<=1 && pks<=1

    clear xx yy2;  
    xx2=-15:interval:15;
    yy2=spline(t2,Ch2,xx2);     
    Position=find(yy2(:)<=-700,1);
    TCh2(kk)=interp1([yy2(Position-1) yy2(Position)],[xx2(Position-1) xx2(Position)],-700)  ;    
    
    Position=find(yy2(:)<=-450,1);
    SkewCh2(kk)=(yy2(Position+10)-yy2(Position-10))/(xx2(Position-10)-xx2(Position+10));
    
%     figure
%     plot(xx2,yy2)
%     hold on;
%     plot(t2,Ch2);
%     stop
    
% % %     if Ch4smoothed(Ebin)<Ch4smoothed(Ebin+100)
    clear xx yy3;
    xx=TCh4(kk)-20:interval:TCh4(kk)+20;
    yy3=spline(t3,Ch3,xx); 
 
%  Position=find(yy3(:)<=-100);  

     try
           Position=length(yy3)-10;
           while yy3(Position)<-80
             Position=Position-1;
           end
                        
           TCh3(kk)=interp1([yy3(Position) yy3(Position+1)],[xx(Position) xx(Position+1)],-80);           
      catch
           TCh3(kk)=1;  % is normally negative therefore +1 will not be seen                                   
      end
%             TCh3(kk)=interp1([yy3(Position-1) yy3(Position)],[xx(Position-1) xx(Position)],-100); 


    TstopE=t1(find(Ch1(:) > Ch1Threshold,1)); 

    if isempty(TstopE)
        TstopE=0;
    end

    if TCh3(kk)==1
        EnergyCh1(kk)=-1;
    else
        if TCh3(kk)<TstopE & TstopE<TCh3(kk)+25
            if Ch1IntegrationWindow < 0
                EnergyCh1(kk)=sum(Ch1(find(Ch1>0)))/length(t1);
            else
                EnergyCh1(kk)=sum(Ch1(find(t1>TCh3(kk) & t1<TCh3(kk) + Ch1IntegrationWindow )))/length(t1);
            end
        else
            EnergyCh1(kk)=0;
        end
          % EnergyCh1(kk)=sum(Ch1(find(Ch1>0)))/length(t1); 
          % EnergyCh1(kk)=max(Ch1); 
    end

    
    

%  xx=t3(1):interval:t3(length(t3));
%  yy3=spline(t3,Ch3,xx);
 
% % %     end
%  
%  stop
%             
    %a=size(AcqVolts1);
    %t=[1:a(1,2)].*0.1;    

%%%%%%%%%%%%%%%%%%%%%%%
if showPulses == 1
    close all hidden;
    figure(1);
    hold on;
    plot(t3,Ch3);
    plot(t4,Ch4);
    % plot(t4,Ch4smoothed);
    plot(t2,Ch2);
    plot(t1,Ch1);
    plot(t1,Ch1smoothed);
    % plot(t2,Ch2smoothed);
    plot(xx,yy3);
    grid on;
    TCh2(kk)
    TCh3(kk)
    w = waitforbuttonpress;    
end
%%%%%%%%%%%%%%%%%%%%%%%       
    
    else
        outtrop=outtrop+1;
% %%%%%%%%%%%%%%%%%%%%%%%    
if showPulses == 1
    close all hidden;
    figure(1);
    hold on;
    plot(t3,Ch3);
    plot(t4,Ch4);
    plot(t4,Ch4smoothed);
    plot(t2,Ch2);
    % plot(t1,Ch1);
    plot(t1,Ch1smoothed);
    % plot(t2,Ch2smoothed);
    plot(xx,yy3);
    grid on;
    TCh2(kk)
    TCh3(kk)
    pksCh1
    w = waitforbuttonpress;    
end
% %%%%%%%%%%%%%%%%%%%%%%%   
    end
     
    
  end
end
    
  
% % %   if -TCh3(kk)<99.3 && -TCh3(kk)>98 && TCh3(kk)~=0
% %     close all hidden;
% %     figure(1);
% %     hold on;
% %     plot(t3,Ch3);
% %     plot(t4,Ch4);
% %     plot(t4,Ch4smoothed);
% %     plot(t2,Ch2);
% %     plot(xx,yy3);
% %     grid on;
% %     axis([-210 100 -900 900]);
% %     TCh2(kk)
% %     TCh3(kk)
% %     w = waitforbuttonpress;         
% % %   end
    end
end %%for kk=1:Arraylength

%%
    close all hidden;
    
    
    [HEy,HEx]=hist(Estart,0:1:1000);
    HEy(1)=0;
    HEy(length(HEy))=0;
    
    figure
    plot(HEx,HEy);
    hold on;
    grid on;
    axis([400 800 0 max(HEy)*1.1]);
        help=lowEcut;
        plot([help help],get(gca,'ylim'),'r','LineWidth',3);
        help=highEcut;
        plot([help help],get(gca,'ylim'),'r','LineWidth',3);       
    
    pause(5);  
    print('-djpeg',[cry_ID 'Energy_Start.jpg']);
    
%     dT=-TCh3;%-TCh3;
    dT=TCh2-TCh3;
    
    [HCh3y,HCh3x]=hist(dT,0:0.5:1500);
    HCh3y(1)=0;
    HCh3y(length(HCh3y))=0;
    
    figure
    semilogy(HCh3x,HCh3y);    
    
    
    [HCh3y,HCh3x]=hist(dT,50:0.02:260);
    HCh3y(1)=0;
    HCh3y(length(HCh3y))=0;
    
    figure
    bar(HCh3x,(HCh3y));   
    axis([min(HCh3x) max(HCh3x) 0 max((HCh3y))*1.1]);
    grid on;
    
    %%
%     All1=dT;
    save([path cry_ID '.txt'],'dT','-ascii')
    save([path cry_ID '_EnergyCh1.txt'],'EnergyCh1','-ascii')
%     save([path cry_ID '_SkewStop.txt'],'SkewCh2','-ascii')
    
    

  
    outtrop/Arraylength*100
%%
  figure;
  EnergyCh1_2=EnergyCh1(find(EnergyCh1~=0));
  [b,ab]=hist(EnergyCh1_2,0.5:0.1:40);
  % [b,ab]=hist(EnergyCh1,0:5:500);
  b(1)=0;
  plot(ab,b);
  grid on;
  axis([0 40 0 max(b)*1.1]);
  % axis([25 500 0 max(b)*1.1]);
% %   pause(5);  
% %    print('-djpeg',[cry_ID 'AmplitudeStop.jpg']);
  
%   figure;
%   scatter(dT,EnergyCh1,1.5);
    %%
    
%%



figure;
hold on;
plot(t3,Ch3);
plot(t4,Ch4);
plot(t4,Ch4smoothed);
plot(t2,Ch2);
plot(xx,yy3);
grid on;

%%
% close all hidden;
%     close figure(1);
    figure;    
    hold on;
    plot(t2,Ch2);
    plot(t3,Ch3);
    plot(t4,Ch4);

    
    
    [HCh4y,HCh4x]=hist(-TCh4,0:0.1:1500);
    HCh4y(1)=0;
    HCh4y(length(HCh4y))=0;
    
    figure
    plot(HCh4x,HCh4y);
    
    %%
    figure;
    [b,a]=hist(SkewCh2,700:1:1300);
    b(length(b))=0;
    b(1)=0;
    bar(a,b);
    
    gpd=find(SkewCh2>900);   
    
    [HCh3y,HCh3x]=hist(dT(gpd),50:0.02:260);
    HCh3y(1)=0;
    HCh3y(length(HCh3y))=0;
   
    
    figure
    bar(HCh3x,HCh3y);   
%     axis([min(HCh3x) max(HCh3x) 0 max((HCh3y))*1.1]);
    axis([98.5 101 0 max((HCh3y))*1.1]);
    grid on;    
    
   figure;
   scatter(SkewCh2,dT,1.5);
   grid on;
    
    
    
    %%    
    
else
    %%

end
