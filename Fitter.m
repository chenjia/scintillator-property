
clear all;
close all;
close all hidden;
clear all;
clearvars all;

%%%%% %%% %%%
%%%%% Time unit --> [ns]
%%%%%
%%%%% IRF:  It's a vector, here in the code. You can put anything you want,
%%%%% actually.
%%%%% %%% riitin
%%%%% 1 or 2 rise times:  Change the fit function in DCF2.m
%%%%% %%% %%%
%%%%% Delay of the IRF:
%%%%%   Basically the delay does not come from an offset of the IRF (e.g.
%%%%%   gaussian centered in 5 ns), but from the range of the x of the IRF.
%%%%%   It is due to the convolution.
%%%%% %%% %%%
%%%%% Careful: the optimisation is fully random (thus being a faithful
%%%%% representation of our modern society). Try more than once, different
%%%%% results for same starting parameters.

% --- I/O OPTIONS

% 
% cry_ID='2023_Sipad_GAGG/gagg1_h5_4474_420longpass_500khz.txt';
% plottitle='GAGG1\_2x2x3\_h5\_4474\_420longpass\_500kHz';
% plotPrefix='plots/h5_4474_420longpass_500kHz';

%  cry_ID='2023_Sipad_GAGG/gagg1_h7_4475_420longpass_500khz.txt';
%  plottitle='GAGG1\_2x2x3\_h7\_4475\_420longpass\_500kHz';
%  plotPrefix='plots/h7_4475_420longpass_500kHz';

%  cry_ID='2023_Sipad_GAGG/gagg1_re_M1_4476_420longpass_500khz.txt';
%  plottitle='GAGG1\_2x2x3\_M1\_4476\_420longpass\_500kHz';
%  plotPrefix='plots/M1_4476_420longpass_500kHz';

%  cry_ID='2023_Sipad_GAGG/gagg1_re_T6_4478_420longpass_500khz.txt';
%  plottitle='GAGG1\_2x2x3\_T6\_4478\_420longpass\_500kHz';
%  plotPrefix='plots/T6_4478_420longpass_500kHz';
%   
%  cry_ID='2023_Sipad_GAGG/gagg1_re_T7_4479_420longpass_500khz.txt';
%  plottitle='GAGG1\_2x2x3\_T7\_4479\_420longpass\_500kHz';
%  plotPrefix='plots/T7_4479_420longpass_500kHz';

%  cry_ID='2023_Sipad_GAGG/gagg1_m3_4477_420longpass_500khz.txt';
%  plottitle='GAGG1\_2x2x3\_m3\_4477\_420longpass\_500kHz';
%  plotPrefix='plots/m3_4477_420longpass_500kHz';


%  cry_ID='2023_Sipad_GAGG/gagg2_h4_4492_420longpass_500khz.txt';
%  plottitle='GAGG2\_2x2x3\_h4\_4492\_420longpass\_500kHz';
%  plotPrefix='plots/GAGG2_fahsdjklfkls_h4_4492_420longpass_500kHz';

%  cry_ID='2023_Sipad_GAGG/gagg2_h7_4493_420longpass_500khz.txt';
%  plottitle='GAGG2\_2x2x3\_h7\_4493\_420longpass\_500kHz';
%  plotPrefix='plots/GAGG2_h7_4493_420longpass_500kHz';

%  cry_ID='2023_Sipad_GAGG/gagg2_re_h7_4493_420longpass_500khz.txt';
%  plottitle='GAGG2\_2x2x3\_h7\_4493\_420longpass\_500kHz';
%  plotPrefix='plots/GAGG2_re_h7_4493_420longpass_500kHz';

%  cry_ID='2023_Sipad_GAGG/gagg2_m2_4494_420longpass_500khz.txt';
%  plottitle='GAGG2\_2x2x3\_m2\_4494\_420longpass\_500kHz';
%  plotPrefix='plots/bad_GAGG2_m2_4494_420longpass_500kHz';

%  cry_ID='2023_Sipad_GAGG/gagg2_m7_4495_420longpass_500khz.txt';
%  plottitle='GAGG2\_2x2x3\_m7\_4495\_420longpass\_500kHz';
%  plotPrefix='plots/GAGG2_m7_4495_420longpass_500kHz';

%  cry_ID='2023_Sipad_GAGG/gagg2_t5_4496_420longpass_500khz.txt';
%  plottitle='GAGG2\_2x2x3\_t5\_4496\_420longpass\_500kHz';
%  plotPrefix='plots/GAGG2_t5_4496_420longpass_500kHz';

 cry_ID='2023_Sipad_GAGG/gagg2_t6_4497_420longpass_500khz.txt';
 plottitle='GAGG2\_2x2x3\_t6\_4497\_420longpass\_500kHz';
 plotPrefix='plots/GAGG2_t6_4497_420longpass_500kHz';

rand("seed", 6)
IRF_filename = "./IRFs/IRF_HPMT_XRays_Conv_2021_12.txt";      % Filename of the IRF as txt file. Used only if IRFGoN==2

benchFormat = 1;    %  0 511keV format, with data in [s], '\n' delimiter, and energy file
%  1 x-ray format, with data in adc units, space delimiter, and no energy file.
x_ray_ConvFactor=0.0130208333; % from [tdc units] to [ns], only used if benchFormat = 1

% - Possibility to subtract a histogram of another measurement
% (background?) from the data histogram. Use it with caution.
subtractHisto = 0;
histogram_to_subtract = "/Users/loris/Documents/CERN_Doctoral/Benches/Data/RisenDecayXRays/BGSO_Early2021/NoSample_220221_HV70_500kHz_Th130_longTest_start_16.49_stop_11.47.txt";
% histogram_to_subtract = "";

% --- FIT OPTIONS
onlyVisualization = 0;      % stop before the fit loop


% WARNING: Normalisation of the abundancies is done as:
% iChma(n)/(iChma(2)+iChma(3)+iChma(4)+iChma(11)) so fix them at 0 if you
% do NOT need them or the abundancies will be wrong!!!
%              tsk,    R1,     R2,     R3,     tau_d1,     tau_d2,     tau_d3,     tau_r1,     tau_r2,     tau_r3,     C_amp
iFitIt= logical([1,     1,      1,      1,          1,          1,         1,          1,          0,          0,          0]);
iChma =         [40,   0.06,   0.68,   0.26,        10,       40,         200,       0.020,        0,          0,          0];
% iFitIt= boolean([1,     1,      1,      1,          1,          1,         1,          0,          0,          0,          0]);
% iChma =         [104.3,   0.01,   0.6,   0.4,         1.6,       26.3 ,         82 ,       0.06,        0,          0,          0];
% iChma =         [38 ,   0.06,  0.5,  0.4,          2,        40 ,       160 ,       0.001,        0,          0,          0];
%  iChma =         [41 ,   0.8,  0.5,  0.5,          4,        20 ,       90 ,       0.7,        1.,          0,          0];

rangedec=650; % fit range config 
% rangedec=150;
rangerise=[39,55];
% rangerise=[100,120];

basfac = 1;


lowEcut=30
highEcut=100

% WARNING: currently with X-Rays data there is a bug using a bin
% width different from the intrinsic TDC binning. Strong aliasing even using multiples of it. Use 0.0130208333.
%t_binWidth = 0.05;
t_binWidth = 0.0130208333;
t_min = 0;
t_max = 650;
t_min_baseline = 0;
t_max_baseline = 30;
t_offset = 0;
IRFGoN=2; % 1: IRF Gaussian then, 2: X-Rays conv HPMT_Laser with TGraphs , 3: combined IRF Pbf2 2x2x3, 4: IRF PbF2 2x2x30 transversal  l   5: FrankenFit  6 FrankenFit 2.0 PbF2 blackpainted    7 FrankenFit 3.0 PbF2 naked
%%
RUNStot=5000;


%    clearvars -except benchFormat x_ray_ConvFactor t_offset t_binWidth t_min t_max t_min_baseline t_max_baseline ict cry_ID plottitle iChma rangedec thirddecy riseyes rangerise_max t_binWidth IRFGoN secdecy lowEcut highEcut basfac fit_cherenkov fit_IRF t_offset;



%%
% fileID = fopen('IRF.txt','w');
% for i=1:length(IRF(:,2))
% fprintf(fileID,'%g \t %g\n',IRF(i,1),IRF(i,2));
% end
% fclose(fileID);
%%






% --- READ DATA ---
if benchFormat == 0
% --- 511 keV bench file format ---
fprintf ('FORMAT USED: 511 keV bench')
pause(3)
% --- read timestamps
data_temp=dlmread(cry_ID,'\n') - t_offset;             % column vector
%    data_temp=dlmread(cry_ID,'\n')*1e-9 - t_offset;             % column vector
size(data_temp)

% --- read energies

cry_ID2=cry_ID(1:length(cry_ID)-4);
energies=dlmread([cry_ID2 '_EnergyCh1.txt'],'\n');

figure;
energies2=energies(find(energies~=0));
h_energy = histogram(energies2,-2:0.1:100);
%  b(1)=0;
%   plot(h_energy);
axis([0 60 0 max(h_energy.Values)*1.1]);
grid on;
hold on;
help=lowEcut;
plot([help help],get(gca,'ylim'),'r','LineWidth',3);
help=highEcut;
plot([help help],get(gca,'ylim'),'r','LineWidth',3);

pause(5);
print('-djpeg',[cry_ID 'AmplitudeStop.jpg']);

selected_energies = find(energies>=lowEcut & energies<=highEcut);   % Cut on the energy

data=data_temp(selected_energies);                                  % Good data selected on the energy


elseif benchFormat == 1
% --- X-Rays bench file format ---
fprintf ('FORMAT USED: X-Rays bench')

% --- read timestamps
data_temp=dlmread(cry_ID,' ')*x_ray_ConvFactor - t_offset;             % column vector
% data_temp=dlmread(cry_ID,' ') - t_offset;             % column vector
% data_temp1=data_temp(:,1);
% data_temp2=data_temp(:,2);
size(data_temp)

pause(3)
data=data_temp;


else
printf ('FORMAT NOT RECOGNIZED. STOP')
pause(3)
stop                % Stop it. Just to be safe.
end

% data=dlmread(cry_ID)*13.02083e-12;



% Histogram timestamps.
%h_time = histogram(data,t_min:t_binWidth:t_max);
%prova = conv(h_time.BinEdges, [0.5 0.5], 'valid');
% hist = histogram(data_temp1, 'BinWidth', t_binWidth, 'BinLimits', [t_min t_max], 'BinCounts', data_temp2);
% DecayCounts = data_temp2;
% DecayBins = data_temp1;
[DecayCounts,DecayBins]=hist(data,t_min:t_binWidth:t_max);

if subtractHisto==1
fprintf("###WARNING: subtracting a histogram from the data!\n");
pause(3);
data_temp=dlmread(histogram_to_subtract,' ')*x_ray_ConvFactor - t_offset;             % column vector
[DecayCounts_sub,DecayBins_sub]=hist(data_temp,t_min:t_binWidth:t_max);
%    DecayBins_sub = DecayBins_sub(find(DecayBins_sub>t_min & DecayBins_sub<t_max));
%    DecayCounts_sub = DecayCounts_sub(find(DecayBins_sub>t_min & DecayBins_sub<t_max))
clear gpd;
gpd=find(DecayBins_sub>t_min_baseline & DecayBins_sub<t_max_baseline); %for baseline
cbaseline=mean(DecayCounts_sub(gpd))+0.001  % Average of the bin content% re-normalization for the minimum baseline
DecayCounts_sub =(DecayCounts_sub - cbaseline);
figure;
hold on;
plot(DecayBins_sub, DecayCounts_sub);
DecayCounts = DecayCounts - DecayCounts_sub;
end

DecayCounts(1)=0;
DecayCounts(length(DecayCounts))=0;










% ------------------------
% --- IRF MANIPULATION ---
if IRFGoN==1
% % %%Gaussian IRF
G = @(x,p) p(1)*exp( - (((x-p(2)).^2)/(2*(p(3).^2))));
%    xIRF=0:1e-3:10;
xIRF=-1:1e-3:1;
%    sig = 0.001;
%    sig = 0.043;
%sig = 0.05243;
sig = 0.06100;
sig = 0.07700;
%sig = 0.0100;
yIRF=G(xIRF,[(1./(sig*sqrt(2.*3.1413))),0,sig]);
%     yIRF=G(xIRF,[1,2,iChma(4)]) + 0.1*G(xIRF,[1,2,iChma(4)*10]);
% %     yIRF=G(xIRF,[1,2,0.063]);
% % %%END: Gaussian IRF

elseif IRFGoN==2
%%% IRF: X-Rays convolved with HPMTandLASER as LUT

%format long
IRF_delay = 0.;              % Shift on the x-axis [ns]
IRF_data = readmatrix(IRF_filename);
yIRF = IRF_data(:, 2) / sum(IRF_data(:, 2));
xIRF = IRF_data(:, 1) + IRF_delay;
xIRF = xIRF.';
yIRF = yIRF.';

% yIRF = interp1(xIRF, yIRF, 0:1e-3:10);       % Linear interpolation between min(xIRF) and max(IRF) with step binning
% xIRF=0:1e-3:10;




%%% END: LUT %%%
%%%%%%%%%%%%%%%%

elseif IRFGoN==3 %old IRF with 1k pole zero combined with 56pF
% % %%%%IRF with exponential tails rising and falling
% % %%old IRF with 1k pole zero plus 56pF real chi2 fit
% % t_d=0.260;
% % R=0.2924;
% % t_r=0.520;
% % R3=0.118;
% % R2=0.5896;
% % tsk=2;
% %
% % xIRF=0:1e-3:10;
% % t=xIRF;
% % gpd=find(t>=tsk);
% % gpd2=find(t<tsk);
% %
% % sig=0.001;
% % zw(gpd)= (R/(t_d).*(exp(-(t(gpd)-tsk)/t_d))+R2/(sig*sqrt(2*3.1415))*exp( - (((t(gpd)-tsk).^2)/(2*sig.^2))))/(R+R2+R3);
% % zw(gpd2)=(R3/(t_r).*(exp((t(gpd2)-tsk)/t_r))+R2/(sig*sqrt(2*3.1415))*exp( - (((t(gpd2)-tsk).^2)/(2*sig.^2))))/(R+R2+R3);
% %
% %  G = @(x,p) p(1)*exp( - (((x-p(2)).^2)/(2*(p(3).^2))));
% %
% % yIRF=conv(G(xIRF,[1,5,0.093/2.35]),zw,'same');
% % %%%%END: IRF with exponential tails rising and falling

%%%%IRF with exponential tails rising and falling
%%old IRF with 1k pole zero and not 56pF
t_d=0.204;
R=0.3964;
t_r=0.483;
R3=0.0927;
R2=0.5109;
tsk=2;

xIRF=0:1e-3:10;
t=xIRF;
gpd=find(t>=tsk);
gpd2=find(t<tsk);

sig=0.001;
zw(gpd)= (R/(t_d).*(exp(-(t(gpd)-tsk)/t_d))+R2/(sig*sqrt(2*3.1415))*exp( - (((t(gpd)-tsk).^2)/(2*sig.^2))))/(R+R2+R3);
zw(gpd2)=(R3/(t_r).*(exp((t(gpd2)-tsk)/t_r))+R2/(sig*sqrt(2*3.1415))*exp( - (((t(gpd2)-tsk).^2)/(2*sig.^2))))/(R+R2+R3);

G = @(x,p) p(1)*exp( - (((x-p(2)).^2)/(2*(p(3).^2))));

yIRF=conv(G(xIRF,[1,5,0.082/2.35]),zw,'same');
%%%%END: IRF with exponential tails rising and falling

% % %%%%IRF with exponential tails rising and falling
% % %old IRF with 1k pole zero plus 56pF sqrt chi2 fit
% % sig=0.001;
% %
% % t_d=0.215;
% % R=0.3843;
% % t_r=0.494;
% % R3=0.1305;%/(2*t_d/(t_r*0.3843));
% % R2=0.4852;
% % tsk=2;
% %
% % xIRF=0:1e-3:10;
% % t=xIRF;
% % gpd=find(t>=tsk);
% % gpd2=find(t<tsk);
% %
% %
% % zw(gpd)= (R/(t_d).*(exp(-(t(gpd)-tsk)/t_d))+R2/(sig*sqrt(2*3.1415))*exp( - (((t(gpd)-tsk).^2)/(2*sig.^2))))/(R+R2+R3);
% % zw(gpd2)=(R3/(t_r).*(exp((t(gpd2)-tsk)/t_r))+R2/(sig*sqrt(2*3.1415))*exp( - (((t(gpd2)-tsk).^2)/(2*sig.^2))))/(R+R2+R3);
% %
% %  G = @(x,p) p(1)*exp( - (((x-p(2)).^2)/(2*(p(3).^2))));
% %
% % yIRF=conv(G(xIRF,[1,5,0.087/2.35]),zw,'same');
% % %%%%END: IRF with exponential tails rising and falling

elseif IRFGoN==4 % IRF with 2x2x30 PbF2 transversal
%%%%IRF with exponential tails rising and falling
t_d=0.545;
R=0.291;
t_r=0.506;
R3=0.1035;
R2=0.6054;
tsk=2;

xIRF=0:1e-3:10;
t=xIRF;
gpd=find(t>=tsk);
gpd2=find(t<tsk);

sig=0.001;
zw(gpd)= (R/(t_d).*(exp(-(t(gpd)-tsk)/t_d))+R2/(sig*sqrt(2*3.1415))*exp( - (((t(gpd)-tsk).^2)/(2*sig.^2))))/(R+R2+R3);
zw(gpd2)=(R3/(t_r).*(exp((t(gpd2)-tsk)/t_r))+R2/(sig*sqrt(2*3.1415))*exp( - (((t(gpd2)-tsk).^2)/(2*sig.^2))))/(R+R2+R3);

G = @(x,p) p(1)*exp( - (((x-p(2)).^2)/(2*(p(3).^2))));

yIRF=conv(G(xIRF,[1,5,0.094/2.35]),zw,'same');
%%%%END: IRF with exponential tails rising and falling
% % % %%%%IRF with exponential tails rising and falling
% % % t_d=0.215;
% % % R=0.3843;
% % % t_r=0.494;
% % % R3=0.1305;
% % % R2=0.4852;
% % % tsk=2;
% % %
% % % xIRF=0:1e-3:10;
% % % t=xIRF;
% % % gpd=find(t>=tsk);
% % % gpd2=find(t<tsk);
% % %
% % % sig=0.001;
% % % zw(gpd)= (R/(t_d).*(exp(-(t(gpd)-tsk)/t_d))+R2/(sig*sqrt(2*3.1415))*exp( - (((t(gpd)-tsk).^2)/(2*sig.^2))))/(R+R2+R3);
% % % zw(gpd2)=(R3/(t_r).*(exp((t(gpd2)-tsk)/t_r))+R2/(sig*sqrt(2*3.1415))*exp( - (((t(gpd2)-tsk).^2)/(2*sig.^2))))/(R+R2+R3);
% % %
% % %  G = @(x,p) p(1)*exp( - (((x-p(2)).^2)/(2*(p(3).^2))));
% % %
% % % yIRF=conv(G(xIRF,[1,5,0.087/2.35]),zw,'same');
% % % %%%%END: IRF with exponential tails rising and falling


elseif IRFGoN==5 %IRF with Cherenkov, linear interpolation of points measured. // L.
yIRF_raw = [0.0000000;0.0020876;0.0419473;0.0319824;0.0369649;0.0568947;0.0618772;0.091772 ;0.131632 ;0.156544 ;0.131632 ;0.0668597;0.0618772;0.0618772;0.027    ;0.017035 ;0.0220175;0.0000000; 0.000];
xIRF_raw = [0.00;
            0.03;
            0.04;
            0.06;
            0.08;
            0.10;
            0.12;
            0.14;
            0.16;
            0.18;
            0.20;
            0.22;
            0.24;
            0.26;
            0.28;
            0.30;
            0.32;
            0.34;
            10.];

xIRF=0:1e-3:10;
yIRF = interp1(xIRF_raw, yIRF_raw, xIRF, 'linear');








elseif IRFGoN==6 %IRF with black-painted PbF2. FrankenFit 2.0 // L.
%        tsk,     tau_d1,     R,    tau_d2,            R2  ,    tau_d3,      R3          , tau_r1
t_d=0.0479;
t_r=0.3739;
R=0.601;
%R2=1.341;
R2=0.8341;
R3=0.284;

tsk=5.;

xIRF=0:1e-3:10;
t=xIRF;
gpd=find(t>=tsk);
gpd2=find(t<tsk);

sig=0.001;
zw(gpd)= (R/(t_d).*(exp(-(t(gpd)-tsk)/t_d))+R2/(sig*sqrt(2*3.1415))*exp( - (((t(gpd)-tsk).^2)/(2*sig.^2))))/(R+R2+R3);
zw(gpd2)=(R3/(t_r).*(exp((t(gpd2)-tsk)/t_r))+R2/(sig*sqrt(2*3.1415))*exp( - (((t(gpd2)-tsk).^2)/(2*sig.^2))))/(R+R2+R3);

G = @(x,p) p(1)*exp( - (((x-p(2)).^2)/(2*(p(3).^2))));

yIRF=conv(G(xIRF,[0.01,2,0.043]),zw,'same');


elseif IRFGoN==7 %IRF with naked PbF2. FrankenFit 3.0 // L.
%        tsk,     tau_d1,     R,    tau_d2,            R2  ,    tau_d3,      R3          , tau_r1
t_d=5.927;
t_r=0.156;
R=0.0289;
R2=0.9689;
R3=0.0022;
% R=0.289;
% R2=0.9689;
% R3=0.0022;

tsk=5.;

xIRF=0:1e-3:10;
t=xIRF;
gpd=find(t>=tsk);
gpd2=find(t<tsk);

sig=0.001;
zw(gpd)= (R/(t_d).*(exp(-(t(gpd)-tsk)/t_d))+R2/(sig*sqrt(2*3.1415))*exp( - (((t(gpd)-tsk).^2)/(2*sig.^2))))/(R+R2+R3);
zw(gpd2)=(R3/(t_r).*(exp((t(gpd2)-tsk)/t_r))+R2/(sig*sqrt(2*3.1415))*exp( - (((t(gpd2)-tsk).^2)/(2*sig.^2))))/(R+R2+R3);

sig_Gauss = 0.043;
%    sig = 0.023;
G = @(x,p) p(1)*exp( - (((x-p(2)).^2)/(2*(p(3).^2))));

yIRF=conv(G(xIRF,[(1./(sig_Gauss*sqrt(2.*3.1413))),2,sig_Gauss]),zw,'same');
yIRF=yIRF/(sum(yIRF)*t_binWidth);

end

% % % %%%%IRF with exponential tails rising and falling
% % %
% % %
% % % t_d=0.204;
% % % R=0.3964;
% % % t_r=0.483;
% % % R3=0.0927;
% % % R2=0.5109;
% % % tsk=2;
% % %
% % % xIRF=0:1e-3:10;
% % % t=xIRF;
% % % gpd=find(t>=tsk);
% % % gpd2=find(t<tsk);
% % %
% % % sig=0.001;
% % % zw(gpd)= (R/(t_d).*(exp(-(t(gpd)-tsk)/t_d))+R2/(sig*sqrt(2*3.1415))*exp( - (((t(gpd)-tsk).^2)/(2*sig.^2))))/(R+R2+R3);
% % % zw(gpd2)=(R3/(t_r).*(exp((t(gpd2)-tsk)/t_r))+R2/(sig*sqrt(2*3.1415))*exp( - (((t(gpd2)-tsk).^2)/(2*sig.^2))))/(R+R2+R3);
% % %
% % %  G = @(x,p) p(1)*exp( - (((x-p(2)).^2)/(2*(p(3).^2))));
% % %
% % % yIRF=conv(G(xIRF,[1,5,0.082/2.35]),zw,'same');
% % % %%%%END: IRF with exponential tails rising and falling



%%%%%%%%%%%%%%%IRF from X-ray tube%%%%%%%%%%%
% % % % tail=0;
% % % % if tail==0
% % % %     G = @(x,p) p(1)*exp( - (((x-p(2)).^2)/(2*(p(3).^2))));
% % % %     xIRF=0:1e-3:10;
% % % %     yIRF=conv(G1(xIRF),G(xIRF,[1,5,0.130/2.35]),'same');
% % % % elseif tail==1
% % % % %     tau1=0.0805;
% % % % %     s1=0.0203;
% % % % %     tsk=5;
% % % % %     G = @(t) (1/(tau1)*(1/2*exp((s1^2-2*t*tau1+2*tau1*tsk)/(2*tau1^2)).*(1-erf((s1^2+tau1*(tsk-t))/(sqrt(2)*s1*tau1)))));
% % % %
% % % %     p=[0.0607    0.0220   5  416.1350  169.2671    0.2448];
% % % %     G = @(t) p(4).*(1./(p(1)).*(1/2*exp((p(2).^2-2*t.*p(1)+2.*p(1).*p(3))./(2.*p(1).^2)).*(1-erf((p(2).^2+p(1).*(p(3)-t))/(sqrt(2).*p(2).*p(1)))))) + ...
% % % %     p(5).*(1./(p(6)).*(1/2*exp((p(2).^2-2*t.*p(6)+2.*p(6).*p(3))./(2.*p(6).^2)).*(1-erf((p(2).^2+p(6).*(p(3)-t))/(sqrt(2).*p(2).*p(6))))));
% % % %
% % % %     xIRF=0:1e-3:10;
% % % % %     yIRF=G1(xIRF);
% % % %     yIRF=conv(G1(xIRF),G(xIRF),'same');
% % % %
% % % %
% % % % % D=dlmread('Laser_int10_elec100mV_OD9.txt')*t_binWidth;
% % % % % [yd,xd]=hist(D,0:t_binWidth:150e-9);
% % % % % xd=xd*1e9;
% % % % % yd(1)=0;
% % % % % yd(length(yd))=0;
% % % % % ydi=interp1(xd,yd,min(xd):1e-3:max(xd));
% % % % % % yIRFi=yIRFi/sum(yIRFi);
% % % % % xIRF=min(xd):1e-3:max(xd);
% % % % %
% % % % % yIRF=conv(G1(xIRF),ydi,'same');
% % % %
% % % %
% % % %
% % % %
% % % % end
%%%%%%%%%%%%%%%End: IRF from X-ray tube%%%%%%%%%%%







% --- Plot the pulse, semilogy
figure;
semilogy(DecayBins,DecayCounts);
title("SemiLogY of the pulse");
grid on;
hold on;
%semilogy(xIRF,yIRF);

figure;
plot(xIRF,(yIRF));
title ("Impulse Response Function (IRF)")
grid on;


xtt=xIRF(find(yIRF>max(yIRF)/2));
IRF_FWHM = (xtt(length(xtt))-xtt(1))*1000;
fprintf("IRF FWHM [ps]: %f\n", IRF_FWHM);
% --- IRF - END ---
% -----------------




%     gpd=find(DecayBins>500*wf & DecayBins<800*wf);  %Window for baseline calculation

% --- Get the bins from histogram instead of the variable which I used to create the histogram. Super. ---
%     DecayBins(find(DecayBins>0,1)+1)-DecayBins(find(DecayBins>0,1))
%     (DecayBins(find(DecayBins>0,1)+10)-DecayBins(find(DecayBins>0,1)))/10
%     DecayBins(find(DecayBins>0,1)+3)-DecayBins(find(DecayBins>0,1)+2)
%     DecayBins(find(DecayBins>0,1)+5)-DecayBins(find(DecayBins>0,1)+4)

binning=(DecayBins(find(DecayBins>0,1)+100)-DecayBins(find(DecayBins>0,1)))/100
%    binning = t_binWidth * 1e9;



% --- Compute and subtract baseline ---
clear gpd;
gpd=find(DecayBins>t_min_baseline & DecayBins<t_max_baseline); %for baseline
cbaseline=mean(DecayCounts(gpd))+0.001  % Average of the bin content% re-normalization for the minimum baseline
cbaseline=cbaseline/(binning);          % Average of the density of events

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
cbaseline=cbaseline/basfac;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

xt =-0:binning:500;             % Create time bins between 0 and 500

% Get the scintillation points
nP = iChma;                     % The parameters
yt = DCF2(nP,xt);               % Pass it to the fit function.
yt = yt/sum(yt);                % Normalise the fit datapoints

% Get the IRF points
yIRFi = interp1(xIRF, yIRF, min(xIRF):binning:max(xIRF));       % Linear interpolation between min(xIRF) and max(IRF) with step binning
yIRFi=yIRFi/sum(yIRFi);         % Normalisation


% Get the fit function points
ct=conv(yIRFi,yt);              % DO THE CONVOLUTION!
% ct=ct/sum(ct);
ctx=0:binning:binning*(length(ct)-1);
y1=ct;%interp1(ctx,ct,xt);      % y1 is the new fit function


DecayCounts1=(DecayCounts - cbaseline*binning);% / binning; %DecayCounts1 are the datapoints without baseline, divided by binning to get the density!


figure;
hold on;
semilogy(DecayBins, DecayCounts1 / (sum(DecayCounts1))); % Plot normalised datapoints density
% semilogy(xt,yIRFi,'g');
semilogy(xt,yt/sum(yt),'b');                        % Plot the scintillation function
semilogy(ctx,y1/sum(y1),'r');                       % Plot the fit function
title("Normalised datapoints, and starting scintillation and pulse");
% figure;
% semilogy(min(xIRF):binning:max(xIRF),yIRFi,'r');
% % plot(y1,'r');



decaybiningfac=1;       % If you want to use different binning for decay time

zw1=find(DecayBins>=0 & DecayBins<=rangedec); % Find all the time bins above 0 and below the give rangedec, i.e. the fit range.
%                 figure; plot(DecayBins(zw1), DecayCounts(zw1));



%                DecayCounts1=DecayCounts-cbaseline*binning*decaybiningfac;
sDC=sum(DecayCounts1(zw1))%-cbaseline)  % New normalisation
%                DecayCounts1=DecayCounts1/sDC;          % Again another normalisation.

xhma=DecayBins(zw1);                    % x in the fit range
yhma=DecayCounts1(zw1)/sDC;             % y normalised in the fit range



%                 iChma=[IRFskewquess,40,0.7,40,IRFsigquess,cbaseline,0.01,fsd,0.1];
% iChma(6)=cbaseline;


Fitjump1=1000;
Fitjump2=100;
iChma1=iChma;
iChma2=iChma;
X1=0;
X2=0;

progress=0;progress_old=-1;

RUNS=RUNStot;
%                 nnnni=1;
X2_i        = zeros(1); % Array to monitor the chi square over time
sel_X2_i    = zeros(1); % Array to monitor the chi square selected over time
sel_X2_iter = zeros(1); % Array to monitor the iterations of the chi square selected over time

if onlyVisualization
    fprintf("onlyVisualization == 1\n STOP NOW\n");
    stop
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% ---   S T A R T   T H E   L O O P   --- %%
for i=1:RUNS
    progress = floor(i/1000);          % Print every 1k runs
    if progress_old ~= progress
        fprintf("Iteration: %i\n", i);
        progress_old = progress;
    end

    % --- Change the parameters to see what happens
    iChma2 = UpdateParameters(iChma1, iFitIt, i, RUNS);

    yhma1 = yhma;
    yhma2 = yhma;

    %%% Get the y values corresponding to parameters iChma1
    xt=xhma;
    yt=DCF2(iChma1,xhma);
    yIRFi=interp1(xIRF,yIRF,min(xIRF):binning:max(xIRF));
    ct=conv(yIRFi,yt);
    ct=ct/sum(ct);
    ctx=0:binning:binning*(length(ct)-1);
    y1=interp1(ctx,ct,xt);
    y1=y1/sum(y1);

    %%% Get the y values corresponding to parameters iChma2
    xt=xhma;
    yt=DCF2(iChma2,xhma);
    yIRFi=interp1(xIRF,yIRF,min(xIRF):binning:max(xIRF));
    ct=conv(yIRFi,yt);
    ct=ct/sum(ct);
    ctx=0:binning:binning*(length(ct)-1);
    y2=interp1(ctx,ct,xt);
    y2=y2/sum(y2);




    % Calculate the chi2 as (...)/sqrt(sigma) --> This is to weigh more the
    % scintillation peak!
    % Just unsure about re-adding the baseline.
    zws=find((y2*sDC+cbaseline*binning)~=0);           % Find the bins with something in
    Nbinzw=y2(zws)*sDC+cbaseline*binning;
    Nzw=sum(Nbinzw);
    chi2=sum((yhma(zws)*sDC-y2(zws)*sDC).^2./sqrt(Nbinzw.*(1-Nbinzw/Nzw)))/length(zws);
%    chi2=sum((yhma(zws)*sDC-y2(zws)*sDC).^2./(Nbinzw.*(1-Nbinzw/Nzw)))/length(zws);


    zws=find((y1*sDC+cbaseline*binning)~=0);
    Nbinzw=y1(zws)*sDC+cbaseline*binning;
    Nzw=sum(Nbinzw);
    chi1=sum((yhma(zws)*sDC-y1(zws)*sDC).^2./sqrt(Nbinzw.*(1-Nbinzw/Nzw)))/length(zws);
%    chi1=sum((yhma(zws)*sDC-y1(zws)*sDC).^2./(Nbinzw.*(1-Nbinzw/Nzw)))/length(zws);

    X2_i (i) = chi2; % daje

    %                  anp=1e-2*(1-i/RUNS);%/(1+exp(1/(1-i/RUNS)));
    anp=0.001*exp(-i/RUNS*50);   % Growth rate for the logistic function (see later)

    % Monitor the chi2


    % % % %old version
    % % %                  chi2=sum(((yhma2-y2).^2./(sqrt((y2*sDC2+iChma2(6)*binning*decaybiningfac))/sDC2)));
    % % %                  chi1=sum(((yhma1-y1).^2./(sqrt((y1*sDC1+iChma1(6)*binning*decaybiningfac))/sDC1)));
    % % %                  anp=1e-8*(1-i/RUNS);%/(1+exp(1/(1-i/RUNS)));
    % % % %end old version

    % % %                  chi2=sum(((yhma2-y2).^2./(((y2*sDC2+iChma2(6)*binning*decaybiningfac))/sDC2)));
    % % %                  chi1=sum(((yhma1-y1).^2./(((y1*sDC1+iChma1(6)*binning*decaybiningfac))/sDC1)));
    % % %                  anp=1e-8*(1-i/RUNS);%/(1+exp(1/(1-i/RUNS)));


    % % % % %                  chi2=sum(((yhma2-y2).^2.*(yhma2)));
    % % % % %                  chi1=sum(((yhma1-y1).^2.*(yhma1)));
    % % % % %                  anp=0.5e-10*(1-i/RUNS);%/(1+exp(1/(1-i/RUNS)));



    %                  chi2=sum((yhma-y2).^2./((y2*sDC+cbaseline*binning*decaybiningfac)/(sDC)));
    %                  chi1=sum((yhma-y1).^2./((y1*sDC+cbaseline*binning*decaybiningfac)/(sDC)));

    %                     chi2=sum((yhma-y2).^2./(y2+1e-5));
    %                     chi1=sum((yhma-y1).^2./(y1+1e-5));

    %                  anp=1e-9*(1-i/RUNS);%/(1+exp(1/(1-i/RUNS)));

    % Use a logistic function to randomly decide if the new chi2 is
    % sufficiently better. Negative growth rate because you want a chi2
    % smaller than before, of course.
    % All this randomisation is... enthralling.
    if  rand(1)<=double(1/(1+exp((chi2-chi1)/anp)))

        chigutdecay=chi2;
        chibaddecay=chi1;

        zws=find((y2*sDC+cbaseline*binning)~=0);
        Nbinzw=y2(zws)*sDC+cbaseline*binning;
        Nzw=sum(Nbinzw);
        chigut=sum((yhma(zws)*sDC-y2(zws)*sDC).^2./(Nbinzw.*(1-Nbinzw/Nzw)))/length(zws);   % Recalculate Chi2 as (...)/sigma
        chierr=sqrt(2/length(zws))*3;

        %                      1/(1+exp((chi2-chi1)/anp))
        %                      X_i(nnnni)=sum((yhma-y2).^2);
        iChma1=iChma2;
        sel_X2_i (i)   = chigutdecay; % daje
        sel_X2_iter(i) = i; % daje
    else
    end

    end

%%% ---   E N D   T H E   L O O P   --- %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%

iChma
nPhma=iChma1

DecayCounts=DecayCounts-cbaseline*binning*decaybiningfac;
sDC=sum(DecayCounts(zw1))%-cbaseline)
DecayCounts=DecayCounts/sDC;
xhma=DecayBins(zw1);
yhma=DecayCounts(zw1);


%                 options=optimset('MaxIter',100000,'MaxFunEvals',100000,'TolFun',1e-15,'TolX',1e-15);
%                 [nPhma,resid,J,Sigma] = nlinfit(xhma,yhma,@DCF2,iChma);
%                 ci = nlparci(nPhma,resid,'jacobian',J);
%                 iChma
%                 nPhma


% falltime=nPhma(2)
% fallabun=nPhma(3)
% %                 fallabun=0;
% falltime2=nPhma(4)
%                 IRFskewquess=nPhma(1);


%                 cbaselinefall=nPhma(4)*sDC;
% % % %                 cbaselinefall=nPhma(4)/binning/100;
cbaselinefall=cbaseline;

x = xhma;%112:binning*100:1400;


xt=x;
yt=DCF2(nPhma,xhma);
yIRFi=interp1(xIRF,yIRF,min(xIRF):binning:max(xIRF));
ct=conv(yIRFi,yt);
ct=ct/sum(ct);
ctx=0:binning:binning*(length(ct)-1);
y=interp1(ctx,ct,xt);
y=y/sum(y);

figure;
plot(x,y);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Chi 2 Plot %%%%%%%%%%%%%%%%%%%%%%%%
%%% Monitor the Chi square vs time. %%%
figure;
iterations = 1:1:length(X2_i);
semilogy(iterations, X2_i)
grid on;
hold on;
scatter(sel_X2_iter, sel_X2_i, 'red', 'filled')
[min_X2, min_X2_i] = min(X2_i);
scatter(min_X2_i, min_X2, 'green', '*');
ylabel('Chi2');
xlabel('Iteration');
title("Chi Square vs # Iteration");
print('-djpeg',[plotPrefix '_Chi2Monitor.jpg']);
print('-dpdf',[plotPrefix '_Chi2Monitor.pdf']);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%P L O T%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%
% --- P R I N T I N G ---
% -- Normalisation of the abundancies
    textDecay1 = ['\tau_d_1=' num2str(nPhma(5),'%6.3f') 'ns   (' num2str((nPhma(2)/(nPhma(2)+nPhma(3)+nPhma(4)+nPhma(11)))*100,'%4.2f %%)')];
    textDecay2 = ['\tau_d_2=' num2str(nPhma(6),'%6.3f') 'ns   (' num2str((nPhma(3)/(nPhma(2)+nPhma(3)+nPhma(4)+nPhma(11)))*100,'%4.2f %%)')];
    textDecay3 = ['\tau_d_3=' num2str(nPhma(7),'%6.3f') 'ns   (' num2str((nPhma(4)/(nPhma(2)+nPhma(3)+nPhma(4)+nPhma(11)))*100,'%4.2f %%)')];
    textCAmp   = ['C_{amp}=' num2str((nPhma(11)/(nPhma(2)+nPhma(3)+nPhma(4)+nPhma(11)))*100,'%4.2f %%')];
% -- Rise times
    textRise1  = ['\tau_r_1=' num2str(nPhma(8)*1000,'%5.0f') 'ps'];
    textRise2  = ['\tau_r_2=' num2str(nPhma(9)*1000,'%5.0f') 'ps'];
    textRise3  = ['\tau_r_3=' num2str(nPhma(10)*1000,'%5.0f') 'ps'];
%%%%%%%%%%%%%%%%%%%%%%%%%


% rangedec=50;
figure;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% DECAY TIME PLOT

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% SUBPLOT1
subplot('position',[0.1 0.25 0.85 0.65]);
% % %                 scatter(xhma,log10(yhma),25,'filled');
% % %                 plot(x,log10(y),'Color',[1 0 0],'LineWidth',2);
plot(x,log10(y*sDC+cbaseline*binning*decaybiningfac),'Color',[1 0 0],'LineWidth',2); %1.3
hold on;
%                 scatter(xhma,(yhma*sDC+cbaseline*binning*decaybiningfac),20,'blue','filled'); %5
%                 bar(xhma,yhma*sDC+cbaselinefall*binning,'c','EdgeColor','c');
%                 hold on;
%                 h=bar(xhma,yhma*sDC+cbaselinefall*binning,'c','EdgeColor','c');
scatter(xhma,log10(yhma*sDC+cbaseline*binning*decaybiningfac),10,'blue','filled'); %DATA POINTS
plot(xhma,log10(smooth(yhma*sDC+cbaseline*binning*decaybiningfac,20)),'Color',[0 0.6 0],'LineWidth',2); %RUNNING AVERAGE
%                scatter(xhma,log10(smooth(yhma*sDC+cbaseline*binning*decaybiningfac,20)),10,'blue','filled'); %5


hold on;
plot(x,log10(y*sDC+cbaseline*binning*decaybiningfac),'Color',[1 0 0],'LineWidth',1.3); %1.3
%                 semilogy(xhma,(yhma),25,'filled');
%                 semilogy(x,(y),'Color',[1 0 0],'LineWidth',2)
grid on;

title([plottitle '  ' num2str(sum(yhma*sDC),'%6.0f') 'counts']);
ylabel('log10(Number of Counts)');
%                      xlabel('\Delta T [ns]');

set(gca,'FontSize',15)
h = get(gca,'ylabel');
set(h,'FontSize',17)
h = get(gca,'xlabel');
set(h,'FontSize',17)
h = get(gca,'title');
set(h,'FontSize',12)
grid off;
%                      helps1= ['\tau_1=' num2str(nPhma(2),'%3.1f') 'ns   R_1=' num2str((1-nPhma(3))*100,'%3.1f') '%'];
%                      annotation('textbox',[0.55 0.6 0.3 0.11],'String',helps1,'FontSize',12,'EdgeColor','none')
%                      helps1= ['\tau_2=' num2str(nPhma(4),'%3.1f') 'ns   R_2=' num2str(nPhma(3)*100,'%3.1f') '%'];
%                      annotation('textbox',[0.55 0.56 0.3 0.11],'String',helps1,'FontSize',12,'EdgeColor','none')

axis([0 rangedec log10(cbaseline*binning*decaybiningfac/2) log10(max(yhma*sDC+cbaseline*binning*decaybiningfac))*21/20]);



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% SUBPLOT2
subplot('position',[0.1 0.1 0.85 0.1]);

testz=zeros(1,length(y));
for iz=1:length(y)
    if y(iz)*sDC+cbaseline*binning*decaybiningfac~=0
    %                         testz(iz)=(yhma(iz)-y(iz))*sDC/max(y*sDC+cbaseline*binning*decaybiningfac)*100;%/y(iz);
        testz(iz)=(yhma(iz)-y(iz))*sDC;
    %                         testz(iz)=(yhma(iz)-y(iz))*sDC/y(iz);
    else
    %                         testz(iz)=(yhma(iz)-y(iz))*sDC/max(y*sDC+cbaseline*binning*decaybiningfac)*100;
        testz(iz)=(yhma(iz)-y(iz))*sDC;
    end
end


plot(xhma,(testz),'blue');
hold on;
plot(xhma,smooth((testz),20),'red','LineWidth',1.3);
hold on;
help=0;
plot(get(gca,'xlim'),[help help],'black','LineWidth',1)
axis([0 rangedec min((testz)) max((testz))]);


%                 axis([80 500 -1000 1000]);
%                      axis([10 rangedec -40 40]);

xlabel('\Delta T [ns]');
%                      ylabel('\Delta log(abs(NoC))');
ylabel('Res. [N. of C.]');

set(gca,'FontSize',15)
h = get(gca,'ylabel');
set(h,'FontSize',17)
h = get(gca,'xlabel');
set(h,'FontSize',17)
h = get(gca,'title');
set(h,'FontSize',17)
grid off;







%%% --- D E C A Y   P L O T   P R I N T I N G ---
annotation('textbox',[0.6 0.78 0.5 0.11],'String',textDecay1,'FontSize',15,'EdgeColor','none')
annotation('textbox',[0.6 0.72 0.5 0.11],'String',textDecay2,'FontSize',15,'EdgeColor','none')
annotation('textbox',[0.6 0.66 0.5 0.11],'String',textDecay3,'FontSize',15,'EdgeColor','none')
pause(5);

%%
print('-djpeg',[plotPrefix '_plot_SingleDecay_res_Decay.jpg']);
print('-dpdf',[plotPrefix '_plot_SingleDecay_res_Decay.pdf']);




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% R I S E   T I M E   P L O T %%%

figure;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% SUBPLOT1
subplot('position',[0.1 0.25 0.85 0.65]);
% % %                 scatter(xhma,log10(yhma),25,'filled');
% % %                 plot(x,log10(y),'Color',[1 0 0],'LineWidth',2);
plot(x,(y*sDC+cbaseline*binning*decaybiningfac),'Color',[1 0 0],'LineWidth',2); %1.3
hold on;
scatter(xhma,(yhma*sDC+cbaseline*binning*decaybiningfac),10,'blue','filled'); %5
% yhma*sDC+cbaseline*binning*decaybiningfac

hold on;
plot(xhma,(smooth(yhma*sDC+cbaseline*binning*decaybiningfac,4)),'Color',[0 0.6 0],'LineWidth',2);
plot(x,(y*sDC+cbaseline*binning*decaybiningfac),'Color',[1 0 0],'LineWidth',2); %1.3




%%% SAVE the datapoints to TXT
% thanks Fiammetta.
xplot = x';
%yplot = ynorm';
% xhma,log10(yhma*sDC+cbaseline*binning*decaybiningfac)
yplot = (yhma*sDC+cbaseline*binning*decaybiningfac)';
% Create a table with the data and variable names
T = table(xplot, yplot);
% Write data to text file
writetable(T, [plotPrefix '_graphsDataPoints.txt'], 'Delimiter',' ');
%%%




%                 semilogy(xhma,(yhma),25,'filled');
%                 semilogy(x,(y),'Color',[1 0 0],'LineWidth',2)
grid on;

% %                 %%%%%%%%%%%%%%%%%%
% %                 plot(x,y*sDC+cbaselinefall*binning,'Color',[1 0 0],'LineWidth',2);
% %                 hold on;
% % %                 scatter(xhma,yhma*sDC+cbaselinefall*binning,15,'blue','filled');
% %                 bar(xhma,yhma*sDC+cbaselinefall*binning,'c','EdgeColor',[0.5 0.5 1]);
% %                 hold on;
% %                 h=bar(xhma,yhma*sDC+cbaselinefall*binning,'c','EdgeColor',[0.4 0.8 1]);
% %                 set(h,'FaceColor',get(h,'EdgeColor'))
% % %                 semilogy(xhma,(yhma),25,'filled');
% % %                 semilogy(x,(y),'Color',[1 0 0],'LineWidth',2)
% %                 grid on;
% %                 plot(x,y*sDC+cbaselinefall*binning,'Color',[0.9 0 0],'LineWidth',3);
% %                 %%%%%%%%%%%%%%%%%%

scatter(xhma,(yhma*sDC+cbaseline*binning*decaybiningfac),10,'blue','filled'); %5
%                plot(xIRF+nPhma(1)+0.013,yIRF/max(yIRF)*max(y*sDC)+cbaseline*binning*decaybiningfac,':','Color',[0.4 0.4 0.4],'LineWidth',1.5);
plot(xIRF-min(xIRF)+nPhma(1)+0.1,yIRF/max(yIRF)*max(y*sDC)+cbaseline*binning*decaybiningfac,':','Color',[0.4 0.4 0.4],'LineWidth',1.5);


title([plottitle '  ' num2str(sum(yhma*sDC),'%6.0f') 'counts']);
ylabel('Number of Counts');
%                      xlabel('\Delta T [ns]');

set(gca,'FontSize',15)
h = get(gca,'ylabel');
set(h,'FontSize',17)
h = get(gca,'xlabel');
set(h,'FontSize',17)
h = get(gca,'title');
set(h,'FontSize',12)
grid off;
%                      helps1= ['\tau_1=' num2str(nPhma(2),'%3.1f') 'ns   R_1=' num2str((1-nPhma(3))*100,'%3.1f') '%'];
%                      annotation('textbox',[0.55 0.6 0.3 0.11],'String',helps1,'FontSize',12,'EdgeColor','none')
%                      helps1= ['\tau_2=' num2str(nPhma(4),'%3.1f') 'ns   R_2=' num2str(nPhma(3)*100,'%3.1f') '%'];
%                      annotation('textbox',[0.55 0.56 0.3 0.11],'String',helps1,'FontSize',12,'EdgeColor','none')

axis([rangerise(1) rangerise(2) 0 (max(yhma*sDC+cbaseline*binning*decaybiningfac))*21/20]);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% SUBPLOT2

subplot('position',[0.1 0.1 0.85 0.1]);

testz=zeros(1,length(y));
for iz=1:length(y)
if y(iz)*sDC+cbaseline*binning*decaybiningfac~=0
%                         testz(iz)=(yhma(iz)-y(iz))*sDC/max(y*sDC+cbaseline*binning*decaybiningfac)*100;%/y(iz);
testz(iz)=(yhma(iz)-y(iz))*sDC;
%                         testz(iz)=(yhma(iz)-y(iz))*sDC/y(iz);
else
%                         testz(iz)=(yhma(iz)-y(iz))*sDC/max(y*sDC+cbaseline*binning*decaybiningfac)*100;
testz(iz)=(yhma(iz)-y(iz))*sDC;
end
end


plot(xhma,(testz),'blue');
hold on;
plot(xhma,smooth((testz),20),'red','LineWidth',1.3);
hold on;
help=0;
plot(get(gca,'xlim'),[help help],'black','LineWidth',1)
axis([rangerise(1) rangerise(2) min((testz)) max((testz))]);


%                 axis([80 500 -1000 1000]);
%                      axis([18 rangerise(2) -40 40]);

xlabel('\Delta T [ns]');
%                      ylabel('\Delta log(abs(NoC))');
ylabel('Res. [N. of C.]');

set(gca,'FontSize',15)
h = get(gca,'ylabel');
set(h,'FontSize',17)
h = get(gca,'xlabel');
set(h,'FontSize',17)
h = get(gca,'title');
set(h,'FontSize',17)
grid off;



%%% --- R I S E   P L O T   P R I N T I N G ---
annotation('textbox',[0.6 0.37 0.5 0.11],'String',textDecay1,'FontSize',15,'EdgeColor','none')
annotation('textbox',[0.6 0.31 0.5 0.11],'String',textDecay2,'FontSize',15,'EdgeColor','none')
annotation('textbox',[0.6 0.26 0.5 0.11],'String',textDecay3,'FontSize',15,'EdgeColor','none')
annotation('textbox',[0.6 0.20 0.5 0.11],'String',textRise1 ,'FontSize',15,'EdgeColor','none')
%annotation('textbox',[0.6 0.29 0.5 0.11],'String',textRise2 ,'FontSize',15,'EdgeColor','none')
    
pause(5);


print('-djpeg',[plotPrefix '_plot_SingleDecay_res_Rise.jpg']);
print('-dpdf',[plotPrefix '_plot_SingleDecay_res_Rise.pdf']);
%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% LOG10 R I S E   T I M E   P L O T %%%

%%
figure;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% SUBPLOT1
subplot('position',[0.1 0.25 0.85 0.65]);
% % %                 scatter(xhma,log10(yhma),25,'filled');
% % %                 plot(x,log10(y),'Color',[1 0 0],'LineWidth',2);
plot(x,log10(y*sDC+cbaseline*binning*decaybiningfac),'Color',[1 0 0],'LineWidth',2); %1.3
hold on;
%                 scatter(xhma,(yhma*sDC+cbaseline*binning*decaybiningfac),20,'blue','filled'); %5
%                 bar(xhma,yhma*sDC+cbaselinefall*binning,'c','EdgeColor','c');
%                 hold on;
%                 h=bar(xhma,yhma*sDC+cbaselinefall*binning,'c','EdgeColor','c');
scatter(xhma,log10(yhma*sDC+cbaseline*binning*decaybiningfac),10,'blue','filled'); %5

hold on;
plot(xhma,log10(smooth(yhma*sDC+cbaseline*binning*decaybiningfac,5)),'Color',[0 0.6 0],'LineWidth',2);
plot(x,log10(y*sDC+cbaseline*binning*decaybiningfac),'Color',[1 0 0],'LineWidth',2); %1.3

scatter(xhma,log10(yhma*sDC+cbaseline*binning*decaybiningfac),10,'blue','filled'); %5

plot(xIRF-min(xIRF)+nPhma(1)+0.013,log10(yIRF/max(yIRF)*max(y*sDC)+cbaseline*binning*decaybiningfac),':','Color',[0.4 0.4 0.4],'LineWidth',1.5);


%                 semilogy(xhma,(yhma),25,'filled');
%                 semilogy(x,(y),'Color',[1 0 0],'LineWidth',2)
grid on;

title([plottitle '  ' num2str(sum(yhma*sDC),'%6.0f') 'counts']);
ylabel('log10(Number of Counts)');
%                      xlabel('\Delta T [ns]');

set(gca,'FontSize',15)
h = get(gca,'ylabel');
set(h,'FontSize',17)
h = get(gca,'xlabel');
set(h,'FontSize',17)
h = get(gca,'title');
set(h,'FontSize',12)
grid off;
%                      helps1= ['\tau_1=' num2str(nPhma(2),'%3.1f') 'ns   R_1=' num2str((1-nPhma(3))*100,'%3.1f') '%'];
%                      annotation('textbox',[0.55 0.6 0.3 0.11],'String',helps1,'FontSize',12,'EdgeColor','none')
%                      helps1= ['\tau_2=' num2str(nPhma(4),'%3.1f') 'ns   R_2=' num2str(nPhma(3)*100,'%3.1f') '%'];
%                      annotation('textbox',[0.55 0.56 0.3 0.11],'String',helps1,'FontSize',12,'EdgeColor','none')

axis([rangerise(1) rangerise(2) 0 log10(max(yhma*sDC+cbaseline*binning*decaybiningfac))*21/20]);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% SUBPLOT2
subplot('position',[0.1 0.1 0.85 0.1]);

testz=zeros(1,length(y));
for iz=1:length(y)
if y(iz)*sDC+cbaseline*binning*decaybiningfac~=0
%                         testz(iz)=(yhma(iz)-y(iz))*sDC/max(y*sDC+cbaseline*binning*decaybiningfac)*100;%/y(iz);
testz(iz)=(yhma(iz)-y(iz))*sDC;
%                         testz(iz)=(yhma(iz)-y(iz))*sDC/y(iz);
else
%                         testz(iz)=(yhma(iz)-y(iz))*sDC/max(y*sDC+cbaseline*binning*decaybiningfac)*100;
testz(iz)=(yhma(iz)-y(iz))*sDC;
end
end


plot(xhma,(testz),'blue');
hold on;
plot(xhma,smooth((testz),20),'red','LineWidth',1.3);
hold on;
help=0;
plot(get(gca,'xlim'),[help help],'black','LineWidth',1)
axis([rangerise(1) rangerise(2) min((testz)) max((testz))]);


%                 axis([80 500 -1000 1000]);
%                      axis([18 rangerise(2) -40 40]);

xlabel('\Delta T [ns]');
%                      ylabel('\Delta log(abs(NoC))');
ylabel('Res. [N. of C.]');

set(gca,'FontSize',15)
h = get(gca,'ylabel');
set(h,'FontSize',17)
h = get(gca,'xlabel');
set(h,'FontSize',17)
h = get(gca,'title');
set(h,'FontSize',17)
grid off;

pause(5);
print('-djpeg',[plotPrefix '_plot_SingleDecay_res_Riselog10.jpg']);
print('-dpdf',[plotPrefix '_plot_SingleDecay_res_Riselog10.pdf']);
%                 stop
% end
%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

figure;
hold on;
scatter(xhma,log10(yhma),25,'filled');
plot(x,log10(y),'Color',[1 0 0],'LineWidth',2);
%                 semilogy(xhma,(yhma),25,'filled');
%                 semilogy(x,(y),'Color',[1 0 0],'LineWidth',2)
% axis([0 rangedec -5 max((log10(y)))]);
grid on;



% output the parameters.
Fcollect=fopen([plotPrefix '_fitout.txt'],'a');
fprintf(Fcollect,'%g \t %g \t %g \t %g \t %g \t %g \t %g \t %g \n',nPhma(1),nPhma(2),nPhma(3),nPhma(4),nPhma(5),nPhma(6),nPhma(7),nPhma(8));
fclose(Fcollect);



figure;
hold on;
scatter(xhma,log10(yhma*sDC+cbaseline*binning*decaybiningfac),25,'filled');
plot(x,log10(y*sDC+cbaseline*binning*decaybiningfac),'Color',[1 0 0],'LineWidth',2);
plot(xhma,log10(smooth(yhma*sDC+cbaseline*binning*decaybiningfac,10)),'Color',[0 0.6 0],'LineWidth',2);
plot(x,log10((y*sDC+cbaseline*binning*decaybiningfac)+sqrt((y*sDC+cbaseline*binning*decaybiningfac))),'Color',[1 0.7 0.7],'LineWidth',2);
plot(x,log10((y*sDC+cbaseline*binning*decaybiningfac)-sqrt((y*sDC+cbaseline*binning*decaybiningfac))),'Color',[1 0.7 0.7],'LineWidth',2);


grid on;


figure;
scatter(xhma,(yhma),25,'filled');
hold on;
plot(x,(y),'Color',[1 0 0],'LineWidth',2)
grid on;


figure;
scatter(xhma,(yhma*sDC+cbaseline*binning*decaybiningfac),25,'filled');
hold on;
plot(x,(y*sDC+cbaseline*binning*decaybiningfac),'Color',[1 0 0],'LineWidth',2);
plot(x,(y*sDC+cbaseline*binning*decaybiningfac)+sqrt((y*sDC+cbaseline*binning*decaybiningfac)),'Color',[1 0.7 0.7],'LineWidth',2);
plot(x,(y*sDC+cbaseline*binning*decaybiningfac)-sqrt((y*sDC+cbaseline*binning*decaybiningfac)),'Color',[1 0.7 0.7],'LineWidth',2);
plot(xhma,smooth(yhma*sDC+cbaseline*binning*decaybiningfac,5),'Color',[0 0.6 0],'LineWidth',2);

% % %                 plot(xtest,(ytest*sDC+cbaseline*binning*decaybiningfac),'Color',[0 0 0],'LineWidth',2);
grid on;


log10(mean(DecayCounts(find(DecayBins>500))))
log10(iChma(4)*binning*decaybiningfac)
log10(nPhma(4)*binning*decaybiningfac)

testz=zeros(1,length(y));
for iz=1:length(y)
if y(iz)*sDC+cbaseline*binning*decaybiningfac~=0
testz(iz)=(yhma(iz)-y(iz));%/y(iz);
else
testz(iz)=(yhma(iz)-y(iz));
end
end

figure
plot(xhma,(testz));
hold on;
plot(xhma,smooth((testz),20),'red','LineWidth',2);
grid on;


%%%%%%%%%%%%%%%%%%%%% E N D %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
fprintf ('The effective decay time is: %f ns \n', 1/(1/nPhma(5)*(nPhma(2)/(nPhma(2)+nPhma(3)+nPhma(4))) + 1/nPhma(6)*(nPhma(3)/(nPhma(2)+nPhma(3)+nPhma(4))) + 1/nPhma(7)*(nPhma(4)/(nPhma(2)+nPhma(3)+nPhma(4))) ) )

fprintf ('minimum chi2 is: %f \n', min(X2_i))